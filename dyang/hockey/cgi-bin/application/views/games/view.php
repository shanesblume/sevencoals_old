<div class="well">
	<p>
	Games may not update immediately after completion. We enter the game winners manually, which is typically done within 
	minutes of the hockey game ending. However, some games (mostly the last game or games of the day) may not be updated 
	until the following morning. Thank you for your understanding.
	</p>
</div>

<button type="button" data-toggle="modal" data-target="#myModal" class="col-xs-3 col-sm-3 col-md-3 btn-picks">Daily Leaderboard</button>

<?php echo form_open('games/view');

	$show_submit = TRUE;
	foreach($games as $game_info)
	{
		if($game_info['date'] < date('Y-m-d'))
		{
			$show_submit = FALSE;
		}
		elseif($game_info['date'] >= date('Y-m-d'))
		{
			$show_submit = TRUE;
		}
	}
	if($show_submit === TRUE)
	{ ?>
		<button type="submit" class="col-xs-3 col-sm-3 col-md-3 col-md-offset-6 btn btn-picks">Submit My Picks</button>;
	<?php }
?>



<div id='picks-list'>
	<table class="well col-xs-12 col-sm-12 col-md-12 table">
		<tr class="table-header">
			<td class="col-xs-2 col-sm-2 col-md-2 text-center">Time</td>
			<td class="col-xs-3 col-sm-3 col-md-3 text-center">Away</td>
			<td class="col-xs-1 col-sm-1 col-md-1 text-center">@</td>
			<td class="col-xs-3 col-sm-3 col-md-3 text-center">Home</td>
			<td class="col-xs-3 col-sm-3 col-md-3 text-center">Status</td>
		</tr>
	</table>
</div>

<?php 
	foreach ($games as $key => $row) 
	{
		$date[$key]  = $row['date'];
		$time[$key] = $row['time'];
	} 
?>

<?php 

	$gameNumber = 0;
	$numberOfGames = 0;
	$no_games = TRUE;
	$date_list = array();
?>

<div id='games-list'>

	<?php foreach ($games as $game_info) 
	{
		$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
		$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
		$currentDate = $game_info['date'];	
		$game_id = $game_info['id'];
		$gameNumber = $gameNumber + 1;
		$numberOfGames = $numberOfGames + 1;
		$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
		$awayPicked = FALSE;
		$homePicked = FALSE;
		$pickResult = 'empty';
		$teamPicked = 'team-picked';
		$pointsPossible = 0;

		if ($game_info['status'] === 'completed') 
		{
			foreach ($picks as $pick_info)
			{

				if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') 
				{
					$awayPicked = TRUE;
				} 

				else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') 
				{
					$homePicked = TRUE;
				}

				if ($awayPicked === TRUE) 
				{
					$pointsPossible = $game_info['homeVotes'];
				} 

				else if ($homePicked === TRUE) 
				{
					$pointsPossible = $game_info['awayVotes'];
				}

				if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === $game_info['winner']) 
				{
					$pickResult = 'correct';
				} 

				else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] !== $game_info['winner'] && !empty($game_info['winner'])) 
				{
					$pickResult = 'incorrect';
				}

			} ?>

			<table class='well col-xs-12 col-sm-12 col-md-12 table'>

				<tr>

					<td class="col-xs-2 col-sm-2 col-md-2">
						<?php echo date("g:iA", strtotime($game_info['time'])) ?>
						<br>
						<span class='glyphicon glyphicon-check'></span>
						<p>Completed</p>
					</td>
					<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
						<div id="logo-wrap">
							<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
						</div>
						<?php echo $game_info['awayTeam'] ?>
						<?php if($game_info['winner'] === 'away'){echo '*';} ?>
					</td> 
					<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>
					<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
						<div id="logo-wrap">
							<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
						</div>
						<?php echo $game_info['homeTeam'] ?>
						<?php if($game_info['winner'] === 'home'){echo '*';} ?>
					</td>
					<td class='col-xs-3 col-sm-3 col-md-3 text-center'
						 <?php if($pickResult === 'empty') { 
						 	echo "id='empty-pick'";
						 } else if ($pickResult === 'correct') {
						 	echo "id='correct-pick'";
						 } 
						 else if ($pickResult === 'incorrect') {
						 	echo "id='incorrect-pick'";
						 }?>>
						 <?php if($pickResult === 'empty') { 
						 	echo "You didn't vote!";
						 } else if ($pickResult === 'correct') {
						 	echo "You earned ".$pointsPossible." points!";
						 } 
						 else if ($pickResult === 'incorrect') {
						 	echo "You missed ".$pointsPossible." points!";
						 }?>
						 <br>
						<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>
					</td>

				</tr>

			</table>
	<?php }} ?>
</div>


<?php $gameNumber = 0; ?>
<?php $numberOfGames = 0; ?>
<?php $no_games =TRUE; ?>

<div id='games-list'>

	<?php 
		foreach ($games as $game_info)
		{
			$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
			$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
			$currentDate = $game_info['date'];	
			$game_id = $game_info['id'];
			$gameNumber = $gameNumber + 1;
			$numberOfGames = $numberOfGames + 1;
			$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
			$awayPicked = FALSE;
			$homePicked = FALSE;
			$pickResult = 'empty';
			$teamPicked = 'team-picked';
			$pointsPossible = 0;
			$pointsEarned = 0;

			if ($game_info['status'] === 'closed') {

				$no_games = FALSE;

				foreach ($picks as $pick_info)
				{
					if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') {
						$awayPicked = TRUE;
					} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') {
						$homePicked = TRUE;
					}

					if ($awayPicked === TRUE) {
						$pointsPossible = $game_info['homeVotes'];
					} else if ($homePicked === TRUE) {
						$pointsPossible = $game_info['awayVotes'];
					}

					if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === $game_info['winner']) {
						$pickResult = 'correct';
					} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] !== $game_info['winner'] && !empty($game_info['winner'])) {
						$pickResult = 'incorrect';
					}
				} ?>

			<div id="picks-list">

				<table class='well col-xs-12 col-sm-12 col-md-12 table'>

					<tr>

						<td class="col-xs-2 col-sm-2 col-md-2">
							<?php echo date("g:iA", strtotime($game_info['time'])) ?>
							<br>
							<span class='glyphicon glyphicon-play-circle'></span> 
							<p>In Progress</p>
						</td>

						<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
							</div>
							<?php echo $game_info['awayTeam'] ?>
						</td> 

						<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>

						<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
							</div>
							<?php echo $game_info['homeTeam'] ?>
						</td>

						<td class='col-xs-3 col-sm-3 col-md-3 text-center'
							 <?php if($pickResult === 'empty') { 
							 	echo "id='current-game'";
							 } else if ($pickResult === 'correct') {
							 	echo "id='current-game'";
							 } 
							 else if ($pickResult === 'incorrect') {
							 	echo "id='current-game'";
							 }?>>
							 <?php echo "Playing for ".$pointsPossible." points."; ?>
							 <br>
							<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>							 	
						</td>

					</tr>

				</table>

			</div>

	<?php }} ?>

</div>


<?php 

	$gameNumber = 0;
	$numberOfPicks = 0;
	$numberOfGames = 0;
	$todaysDate = date('Y-m-d');
	$todaysTime = date('H:i:s');

?>

<div id='games-list'>

	<?php 

		foreach ($games as $game_info) 
		{

			$away_logo_url = base_url('/assets/img/team_logos').'/'.strtolower($game_info['awayTeam']).'_logo.png';
			$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
			$game_id = $game_info['id'];
			$gameNumber = $gameNumber + 1;
			$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
			$awayPicked = FALSE;
			$homePicked = FALSE;
			$teamPicked = 'team-picked';

			foreach ($picks as $pick_info) 
			{
				if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') 
				{
					$awayPicked = TRUE;
				} 
				else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') 
				{
					$homePicked = TRUE;
				}
			}

			if( ($game_info['date'] > $todaysDate) || ($game_info['date'] === $todaysDate && date('H:i:s', strtotime($game_info['time'])) > $todaysTime) ) 
			{
				$numberOfPicks = $numberOfPicks + 1;
				$numberOfGames = $numberOfGames + 1;
				echo form_hidden('username', $this->tank_auth->get_username());
				echo form_hidden('game_id_'.$gameNumber, $game_info['id']);		
				echo form_hidden('number_of_games', $numberOfGames);
				echo form_hidden('game_date_'.$gameNumber, date('Y-m-d', strtotime($game_info['date'])));
				echo form_hidden('game_time_'.$gameNumber, date('H:i:s', strtotime($game_info['time'])));

				?>

				<table class='well col-xs-12 col-sm-12 col-md-12 table'>

					<tr>

						<td class="col-md-2">
							<?php echo date("g:iA", strtotime($game_info['time'])) ?>
							<br>
							<span class='glyphicon glyphicon-pencil'></span>
							<p>Still Open</p>
						</td>

						<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
							<label for="away_<?php echo $gameNumber ?>">
								<div id="logo-wrap">
									<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
								</div>
							</label><br>
							<input id="away_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="away" <?php if($awayPicked === TRUE) {echo "checked";} ?>>
							<?php echo $game_info['awayTeam'] ?>
						</td> 
						<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>
						<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
							<label for="home_<?php echo $gameNumber ?>">
								<div id="logo-wrap">
									<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
								</div>
							</label><br>
							<input id="home_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="home" <?php if($homePicked === TRUE) {echo "checked";} ?>>
							<?php echo $game_info['homeTeam'] ?>
						</td>
						<td class='col-xs-3 col-sm-3 col-md-3 text-center' id="future-pick">
							<?php echo "Pick is still open."; ?>
							<br>
							<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>
						</td>

					</tr>

				</table>

		<?php }} ?>

</div>

<?php if($numberOfPicks > 0 ) { echo '<button type="submit" class="col-xs-3 col-sm-3 col-md-3 col-md-offset-9 btn btn-picks">Submit My Picks</button>' ;} ?>

<?php echo form_close() ?>






<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo"Point Totals for ".date("l M. d, Y", strtotime($currentDate)) ?></h4>
      </div>
      <div class="modal-body">
        
                <table class='table table-striped'>

                  <?php $users = array(); ?>
                  <?php $points = array(); ?>
                  <?php $result = array(); ?>

                  <?php foreach($standings as $standings_info) 
                  {
                    foreach($games as $game_info)
                    {
                      if($game_info['id'] === $standings_info['game_id'])
                      {
                        $username = $standings_info['username'];
                        if(!in_array($username, $users)) 
                        {
                           array_push($users, $username);
                        }
                      }
                     
                    }
                  }

                  foreach($users as $user) {

                    $total_pts = 0;

                    foreach($standings as $standings_info) 
                    {
                      foreach($games as $game_info)
                      {
                        if($game_info['id'] === $standings_info['game_id'])
                        {
                          if($user === $standings_info['username'] && date('W',strtotime($game_info['date'])) === date('W'))
                          {
                            $total_pts = $total_pts + $standings_info['pts_earned'];
                          }
                        }
                      }
                    }
                    array_push($points, $total_pts);
                  }

                  for($i=0;$i<count($users);$i++) {
                    $result[$users[$i]] = $points[$i];
                  }

				arsort($result);
				$position = 1;
				foreach($result as $user_key=>$points_value)
			    {
			    	if($user_key === $this->tank_auth->get_username()) {
				    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
					} else {
					    echo "<tr>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
				    }
				    $position = $position + 1;
			    }
				?>

                </table>
                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





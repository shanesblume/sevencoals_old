
<?php 

	$days_list = array();

	foreach($days as $day_info)
	{
		if(!in_array($day_info['date'], $days_list) &&  date('W',strtotime($day_info['date'])) === date('W', strtotime("+1 week")))
		{
			array_push($days_list, $day_info['date']);
		}
	}

	for($i=0;$i<count($days_list);$i++)
	{
		$number_of_games = 0;
		foreach($days as $day_info)
		{
			if($day_info['date'] === $days_list[$i])
			{
				$number_of_games = $number_of_games + 1;
			}

		}
		$days_list[$i] = array($days_list[$i] => $number_of_games);
	}

	$pick_dates_list = array();

	foreach($picks as $pick_info)
	{
		foreach($games as $game_info)
		{
			if($game_info['id'] === $pick_info['game_id'])
			{
				$pick_date = $game_info['date'];
				array_push($pick_dates_list, $pick_date);
			}
		}
	}

	foreach($games as $game_info)
	{
		if($game_info['date'] < date('Y-m-d'))
		{
			$submit_message = 'Results';
		}
		else
		{
			$submit_message = 'View Games';
		}
	}

?>

<div id="picks-list">

	<table class="well col-xs-12 col-sm-12 col-md-12 table">
		<tr class="table-header">
			<td class="col-xs-1 col-sm-1 col-md-1 text-center">Status</td>
			<td class="col-xs-2 col-sm-2 col-md-2 text-center">Action</td>
			<td class="col-xs-6 col-sm-6 col-md-6">Date</td>
			<td class="col-xs-3 col-sm-3 col-md-3 text-center">Picks Made</td>
		</tr>
	</table>

	<?php 
		foreach($days_list as $day) 
		{
			foreach($day as $games_in_day) 
			{
				$picks_in_day = count(array_keys($pick_dates_list, key($day)));
				if($games_in_day === $picks_in_day) 
					{
						$day_style = "finished";
					}
				if($games_in_day > $picks_in_day) 
					{
						$day_style = "unfinished";
					}
				if(key($day) < date('Y-m-d'))
					{
						$submit_message = 'View Results';
						$glyphicon = "<span class='glyphicon glyphicon-check'></span>";
						$status_message = "<span style='font-size:.5em;'>Completed</span>";
					}
				if(key($day) >= date('Y-m-d'))
					{
						$submit_message = 'View Games';
						$glyphicon = "<span class='glyphicon glyphicon-pencil'></span>";
						$status_message = "<span style='font-size:.5em;'>Open</span>";
					} 
				if(key($day) === date('Y-m-d'))
					{
						$submit_message = 'View Games';
						$glyphicon = "<span class='glyphicon glyphicon-chevron-right'></span> ";
						$status_message = "<span style='font-size:.5em;'>Current</span>";
					}
	?>

				<table class="well col-xs-12 col-sm-12 col-md-12 table">
					<tr>
						<td class="col-xs-1 col-sm-1 col-md-1 text-center"><?php echo $glyphicon.'<br>'.$status_message ?></td>
						<td class="col-xs-2 col-sm-2 col-md-2 text-center"><a href="<?php echo site_url('games/'.key($day)) ?>"><button type="submit" class="btn"><?php echo $submit_message ?></button></a></td>
						<td class="col-xs-6 col-sm-6 col-md-6" id="picks-date"><?php echo date("l M. d, Y", strtotime(key($day))) ?></td>
						<td class="col-xs-3 col-sm-3 col-md-3 text-center" id="<?php echo $day_style.'-picks-numbers'?>"><?php echo $picks_in_day." / ".$games_in_day ?></td>
					</tr>
				</table>

			<?php } ?>
		<?php } ?>

</div> 
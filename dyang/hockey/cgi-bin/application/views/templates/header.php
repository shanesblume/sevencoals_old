<DOCTYPE HTML>
<html>
<head>
  <!-- Meta -->
	<meta charset=utf-8>
  <meta name="description" content="An NHL hockey pick'em game with a unique scoring system. It's a free, competitive way to prove your hockey knowledge.">
  <!-- Title -->
	<title><?php echo $title ?> - Hockey Pick'em</title>
	<!-- Styles -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/css/style.css') ?>">
  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>

  <link rel="icon" href="<?=base_url()?>/favicon.png" type="image/png">

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-47684602-1', 'sevencoals.com');
  ga('send', 'pageview');
  </script>

</head>

<body>

<!-- Set Timezone -->
<?php date_default_timezone_set('America/New_York'); ?>

<!-- Header -->
<div class="page-header text-center" id="header">

  <!-- Header Text -->
	<h3>SevenCoals Presents:</h3>
  <h2>Hockey Pick'em</h2>

      <!-- Navbar -->
      <div class="navbar navbar-default" role="navigation">

        <!--  Navbar Collapse -->
        <div class="navbar-collapse collapse">

          <ul class="nav navbar-nav">
            <li><a href="<?php echo site_url() ?>">Home</a></li>
            <li><a href="<?php echo site_url('pages/how_to_play') ?>">How to Play</a></li>
            <li><a href="<?php echo site_url('pages/news') ?>">News and Updates</a></li>
          </ul>

          <?php if(!$this->tank_auth->is_admin()) { ?>
            <ul class="nav navbar-nav">
              <li><a href="<?php echo site_url('games/picks') ?>">My Picks</a></li>
            </ul>
          <?php } ?>

          <ul class="nav navbar-nav">
            <li><a href="<?php echo site_url('games/standings') ?>">Leaderboards</a></li>
            <li><a href="<?php echo site_url('games/stats') ?>">User Stats</a></li>
            <li><a href="<?php echo site_url('pages/contact') ?>">Feedback</a></li>
          </ul>

          <?php if($this->tank_auth->is_admin()) { ?>
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin Menu <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('games/update_results') ?>">Update Results</a></li>
                  <li><a href="<?php echo site_url('games/create_games') ?>">Create Games</a></li>
                </ul>
              </li>
            </ul>
          <?php } ?>

          <?php if($this->tank_auth->is_logged_in()) { ?>
            <ul class="nav navbar-nav pull-right" id="user-menu">
              <p class="navbar-text" style="color: #B8BDA6"><b><em>Signed is as: <?php echo "<span style='color:white'>".$this->tank_auth->get_username()."</span>" ?></em></b></p>
              <li><a href="<?php echo site_url('auth/logout') ?>">Logout</a></li>
            </ul>
          
          <?php } else { ?>
            <ul class="nav navbar-nav pull-right" id="user-menu">
              <p class="navbar-text" style="color:#B8BDA6"><b>You're not signed in:</b></p>
              <li><a href="<?php echo site_url('auth/login') ?>">Login</a></li>
              <li><a href="<?php echo site_url('auth/register') ?>">Register</a></li>
            </ul>
          <?php } ?>    

        </div> <!-- End Navbar Collapse -->

      </div> <!-- End Navbar -->

</div> <!-- End Header -->

<!--Content -->
<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-6 col-md-push-3">

    

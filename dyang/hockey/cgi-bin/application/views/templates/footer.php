    </div>

    <div class="well col-xs-12 col-sm-12 col-md-2 col-md-pull-5" id="navigation">

        <ul class="nav nav-list">
          <li><b>Main Menu</b></li>
            <ul>
              <li><a href="<?php echo site_url() ?>">Home</a></li>
              <li><a href="<?php echo site_url('pages/news') ?>">News and Updates</a></li>
              <li><a href="<?php echo site_url('pages/contact') ?>">Help make it better</a></li>
              <li>About us</li>
            </ul>
            <br>
          <li><b>New Users</b></li>
            <ul>
              <li><a href="<?php echo site_url('pages/how_to_play') ?>">How do I play?</a></li>
              <li>Where do I start?</li>
              <li>Detailed Rules List</li>
              <li><a href="<?php echo site_url('auth/register') ?>">Sign me up</a></li>
            </ul>
            <br>
          <li><b>Picks and Results</b></li>
            <ul>
              <li><a href="<?php echo site_url('games/'.date('Y-m-d'))?>">Today's Games</a></li>
              <li><a href="<?php echo site_url('games/'.date('Y-m-d', strtotime('-1 day')))?>">Yesterday's Results</a></li>
              <li><a href="<?php echo site_url('games/picks')?>">Week View</a></li>
            </ul>
            <br>
          <li><b>Research</b></li>
            <ul>
              <li><a href="<?php echo site_url('games/current_weeks_standings')?>">Current Weekly Standings</a></li>
              <li><a href="<?php echo site_url('games/last_weeks_standings')?>">Last Week's Results</a></li>
              <li><a href="<?php echo site_url('games/season_standings')?>">Current Season Standings</a></li>
              <li><a href="<?php echo site_url('games/user/'.$this->tank_auth->get_username())?>">My Stats</a></li>
            </ul>
        </ul>

    </div>

    <div class="col-md-2 col-md-push-1 text-center">
      <h4>Follow us on Twitter!</h4>
      <a href="https://twitter.com/SevenCoals" class="twitter-follow-button" data-show-count="false" data-size="large" id="twitter-button">@SevenCoals</a>
    </div>



<div id="sitelock_shield_logo" class="fixed_btm" style="bottom:0;position:fixed;_position:absolute;right:0;"><a href="https://www.sitelock.com/verify.php?site=sevencoals.com" onclick="window.open('https://www.sitelock.com/verify.php?site=sevencoals.com','SiteLock','width=600,height=600,left=160,top=170');return false;" ><img alt="PCI Compliance and Malware Removal" title="SiteLock"  src="//shield.sitelock.com/shield/sevencoals.com"/></a></div>

<!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="assets/js/script.js"></script>
<script>
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
  if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}
  (document, 'script', 'twitter-wjs');
</script>


</body>
</html>
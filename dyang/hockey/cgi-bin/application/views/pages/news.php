<div class="">

	<h3>New Picks Available on Sundays</h3>
	<p>
		Wether you're a weekend warrior or just trying to squeeze as much hockey into the weekend as you can you probably
		don't want to have to make next week's picks on Monday afternoon. So we've added the ability to make next week's
		picks starting first thing Sunday morning. Just go to "My Picks" and click on the "Next Week's Picks" link at the 
		top of the page (available only on Sundays). For scoring purposes weeks will still be starting on Monday. We're just 
		granting everyone VIP early access.
	</p>

</div>

<div class="alert-warning">

	<h3>ALERT: Late Game Issues</h3>
	<p>
		We have experienced some issues with changing picks for later games after earlier games have already started.
		We are working on a fix but some games may lock after the first puck drops for the day. We'll update you when it's
		corrected but in the meantime, your initial gut feeling was probably correct anyway.
	</p>

	<div class="alert alert-success">

		<h3>UPDATE: Issue Resolved</h3>
		<p>
			You can now pick late games with confidence and up to a second before a game starts. 
			If this issue affected your picks in any way we apologize, but seriously...you need to stop procrastinating. 
		</p>

	</div>

</div>

<div class="">

	<h3>Twitter Users, Listen Up!</h3>
	<p>
		If you're an active Twitter user be sure to follow @sevencoals. We'll be announcing impressive picks, site updates, and stats that may
		help you make smarter choices in the game and in life. Search for us on Twitter or use the link on
		this site. We could use any support we can get! <b>#YOLO #AmIDoingThisRight?</b>  
	</p>

</div>

<div class="well col-md-12">

	<?php echo form_open('email'); ?>

		<input type="hidden" name="username" value="<?php echo $this->tank_auth->get_username() ?>">

		<div>

			<div class="row">
				<span class='text-center'><h4>Have a comment, question, or suggestion for SevenCoals?</h4></span>
				<span class="text-center"><p>NOTE: This will send a message directly to the site's administrators. It will not be a public post.</p></span>

				<?php 
				$contact_type = array(
                  'comment'    => "I'd like to make a comment: ",
                  'suggestion'   => "I have a suggestion for the site: ",
                  'question'  => "I have a question I need answered: ",
                  'report'   => "Something's broken, fix it: ",
                );

				echo form_dropdown('contact_type', $contact_type);
				?>

				<textarea class="col-xs-12 col-sm-12 col-md-12 panel" rows="5" name="message"></textarea>
				<span class="pull-right"><button type="submit" class="btn btn-picks">Send</button></span>

			</div>

		</div>

	<?php echo form_close() ?>

	<div class="well">
		<p>Along with sending us your questions and comments we would also appreciate it if you participated in our poll. These will change 
		often to reflect areas of the site that we are considering working on. Our vision is a site that delivers a game that you want to play 
		rather than making you conform to what's being offered. Fight the man and make your voice heard!
		</p>
		<h3>SevenPolls</h3>
		<p>
			<b>We have considered offering an Amazon.com Gift Card to our weekly winners. Would you prefer:<br></b>
		</p>
		<p>
			<?php echo form_open('pages/home'); ?>
				<?php 
				if($this->tank_auth->is_logged_in())
				{
					$username = $this->tank_auth->get_username();
				}else{
					$username = 'NA';
				} ?>
				<?php echo form_hidden('username', $username); ?>
				<?php echo form_hidden('poll_name', 'Prize Delivery Method'); ?>
				<input id="SevenPolls" type="radio" name="result" value="Electronic"> An electronic card sent to my email, because timing is everything.<br>
				<input id="SevenPolls" type="radio" name="result" value="Mailed"> A traditional card through the mail, because everyone loves getting mail.<br> 
				<input id="SevenPolls" type="radio" name="result" value="Other"> I don't win so I my input shouldn't matter. Thanks for the awesome site though!
		</p>

		<span><button type="submit" class="btn btn-picks">Hear My Voice!</button></span>
		<?php echo form_close() ?>

	</div>

</div>
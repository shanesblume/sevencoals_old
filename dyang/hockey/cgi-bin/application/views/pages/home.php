
<div class='col-md-12'>
<?php $users = array(); ?>
<?php $points = array(); ?>
<?php $result = array(); ?>


	      <?php foreach($standings as $standings_info) 
	      {
	        foreach($games as $game_info)
	        {
	          if($game_info['id'] === $standings_info['game_id'] && date('W',strtotime($game_info['date'])) === date('W', strtotime("-1 week")))
	          {
	            $username = $standings_info['username'];
	            if(!in_array($username, $users)) 
	            {
	               array_push($users, $username);
	            }
	          }
	         
	        }
	      }

          foreach($users as $user) {

            $total_pts = 0;

            foreach($standings as $standings_info) 
            {
              foreach($games as $game_info)
              {
                if($game_info['id'] === $standings_info['game_id'])
                {
                  if($user === $standings_info['username'] && date('W',strtotime($game_info['date'])) === date('W', strtotime("-1 week")))
                  {
                    $total_pts = $total_pts + $standings_info['pts_earned'];
                  }
                }
              }
            }
            array_push($points, $total_pts);
          }

		for($i=0;$i<count($users);$i++) {
			$result[$users[$i]] = $points[$i];
		}

		arsort($result);
		$position = 1;
		$winning_score = 0;
		$user_points = 0;


		?>


		<div class="row">

			<div class="col-md-12 well text-center" id="week-champion">

				<?php foreach($result as $user_key=>$points_value)
				{
					if ($points_value >= $winning_score)
					{ ?>
						<p>
							<b>Congratulations to last week's champion <a href="<?php echo site_url('games/user/'.$user_key)?>"><?php echo $user_key?></a> who finished with <?php echo $points_value ?> points!</b>	
							<?php $winning_score = $points_value;
					}

			    } ?>
			    	<br>
			    	<em>Think you can do better? <a href="<?php echo site_url('auth/register') ?>">Sign up</a> and start making some picks! If you're already a member... try harder?</em>
			    </p>

			</div>

		</div>

		<div class="col-md-12 well">
			<h1>What is SevenCoals?</h1>
			<p>
				It's a <b>free NHL hockey pick'em</b> game with a <b>unique scoring system</b> that rewards you for calling games that no one else could. You pick the winners of
				games and we reward you with points that are... kind of meaningless right now (but that may be changing). A <b>new champion is crowned every week</b>
				so it's never too late to join. 
			</p>
			<p>
				We've learned that there is more to hockey than watching your home team play on a channel that 
				you can barely find. Or staring in awe as your fantasy hockey team get's destroyed by injuries and goalie changes.  
				With SevenCoals you can have a vested interest in every single NHL game. Our complicated scoring algorithm* will let you 
				know if you're a hockey predicting
				savant or just another Average Joe picking wins that everyone else saw coming. Get started by 
				<a href="<?php echo site_url('auth/register') ?>">signing up</a> or <a href="<?php echo site_url('auth/login') ?>">signing in</a> and start making some picks! 
			</p>
			<p style="font-size:.8em" class="text-center">
				<em>*Complicated scoring algorithm is actually very simple, read more about it in the <a href="<?php echo site_url('pages/how_to_play') ?>">How to Play</a> section.</em> 
			</p>

		</div>

		<div class="row well">

			<div class="col-md-6">

				<h3>New Picks Available on Sundays</h3>
				<p>
					Wether you're a weekend warrior or just trying to squeeze as much hockey into the weekend as you can you probably
					don't want to have to make next week's picks on Monday afternoon. So we've added the ability to make next week's
					picks starting first thing Sunday morning. Just go to "My Picks" and click on the "Next Week's Picks" link at the 
					top of the page (available only on Sundays). For scoring purposes weeks will still be starting on Monday. We're just 
					granting everyone VIP early access.
				</p>

			</div>

			<div class="col-md-5 col-md-push-1" id="SevenPolls">
				<h3>SevenPolls</h3>
				<p>
					<b>We have considered offering an Amazon.com Gift Card to our weekly winners. Would you prefer:<br></b>
				</p>
				<p>
					<?php echo form_open('pages/home'); ?>
						<?php 
						if($this->tank_auth->is_logged_in())
						{
							$username = $this->tank_auth->get_username();
						}else{
							$username = 'NA';
						} ?>
						<?php echo form_hidden('username', $username); ?>
						<?php echo form_hidden('poll_name', 'Prize Delivery Method'); ?>
						<input id="SevenPolls" type="radio" name="result" value="Electronic"> An electronic card sent to my email, because timing is everything.<br>
						<input id="SevenPolls" type="radio" name="result" value="Mailed"> A traditional card through the mail, because everyone loves getting mail.<br> 
						<input id="SevenPolls" type="radio" name="result" value="Other"> I don't win so I my input shouldn't matter. Thanks for the awesome site though!
				</p>

				<span><button type="submit" class="btn btn-picks">Hear My Voice!</button></span>
				<?php echo form_close() ?>

			</div>

		</div>

		<div class="row well">

			<div class="col-md-12 alert alert-success">

				UPDATE: The late picks issue has been RESOLVED! You can now pick late games with confidence and up to a second before a game starts. 
				If this issue affected your picks in any way we apologize, but seriously...you need to stop procrastinating. For more information check
				out the <a href="<?php echo site_url('pages/news') ?>">News and Updates</a> page.
		
			</div>

	</div>
			
</div>





<div class="col-md-12" id="contact">

	<?php echo form_open('email'); ?>

		<input type="hidden" name="username" value="<?php echo $this->tank_auth->get_username() ?>">

		<div>

			<div class="row">
				<span class='text-center'><h4>Have a comment, question, or suggestion for SevenCoals?</h4></span>
				<span class="text-center"><p>NOTE: This will send a message directly to the site's administrators. It will not be a public post.</p></span>

				<?php 
				$contact_type = array(
                  'comment'    => "I'd like to make a comment: ",
                  'suggestion'   => "I have a suggestion for the site: ",
                  'question'  => "I have a question I need answered: ",
                  'report'   => "Something's broken, fix it: ",
                );

				echo form_dropdown('contact_type', $contact_type);
				?>

				<textarea class="col-xs-12 col-sm-12 col-md-12" rows="5" name="message"></textarea>
				<span class="pull-right"><button type="submit" class="btn btn-picks">Send</button></span>

			</div>

		</div>

	<?php echo form_close() ?>

</div>


<div class="col-md-12" id="contact">

	<div class="">
		<p>Along with sending us your questions and comments we would also appreciate it if you participated in our poll. These will change 
		often to reflect areas of the site that we are considering working on. Our vision is a site that delivers a game that you want to play 
		rather than making you conform to what's being offered. Fight the man and make your voice heard!
		</p>
		<h4>SevenPolls</h4>
		<p>
			<b>In a terrible alternate universe where hockey doesn't exist, I enjoy following:<br></b>
		</p>
		<p>
			<?php echo form_open('pages/home'); ?>
				<?php 
				if($this->tank_auth->is_logged_in())
				{
					$username = $this->tank_auth->get_username();
				}else{
					$username = 'NA';
				} ?>
				<?php echo form_hidden('username', $username); ?>
				<?php echo form_hidden('poll_name', 'Next Sport'); ?>
				<input id="SevenPolls" type="radio" name="result" value="Soccer"> World Cup Soccer. It's all about the national pride.<br>
				<input id="SevenPolls" type="radio" name="result" value="Basketball"> March Madness Basketball. Go local state college!<br> 
				<input id="SevenPolls" type="radio" name="result" value="Hockey"> My friends off a bridge, because life without hockey is unacceptable.
		</p>

		<span><button type="submit" class="btn btn-picks">Hear My Voice!</button></span>
		<?php echo form_close() ?>

	</div>

</div>
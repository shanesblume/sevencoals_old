<div class="col-md-12" id="faq-container">

	<div class="row">

		<div class="col-md-12" id="faq-convo">

			<h4>What is a hockey pick'em?</h4>

			<p>
				In a hockey pick'em you <b>try to predict which team is going to
				win an NHL hockey game</b> based on research, hockey smarts, a gut feeling, or
				good ol' reliable luck. Your picks are matched 
				up against other players to see who does better.
			</p>

			<h4>Wow, that sounds...bland. And I've seen it before. Make it better.</h4>

			<p>
				We did! With SevenCoals Hockey Pick'em <b>every game is worth a variable 
				amount of points</b> depending on how many people voted on a game and whether
				or not you are voting with the minority or the majority. If you correctly
				predict the winner of a game you get <b>one point for each person that picked
				the losing team</b>.
			</p>

			<h4>Sorry, I got distracted. Can you clarify that?</h4>

			<p>
				Absolutely. Let's say the Buffalo Sabres are playing the Pittsburgh Penguins. 
				100 people made picks and out of those 100 picks submitted, 80 users picked 
				Pittsburgh as the winner while the other 20 people picked Buffalo.
			</p>

			<h4>Wait... stop right there. Do the people who picked Buffalo even watch hockey?</h4>

			<p>
				It's just an example, keep reading. If Pittsburgh wins, anyone who picked 
				them would get 20pts (because 20 people picked Buffalo) and everyone who picked Buffalo 
				would get 0pts (because they were wrong). But if Buffalo wins the 20 people that picked them
				would get 80pts each (beacuse 80 people picked Pittsburgh) and the 80 people that 
				picked Pittsburgh would get 0pts each (beacuse they were wrong).
			</p>

			<h4>Okay, so I should always vote for the underdog?</h4>

			<p>
				Not at all. Even though <b>you don't lose any points for an incorrect pick</b>, you don't
				gain any either. You'll have to decide if the reward of more points is worth the 
				risk of getting none at all. Oh, and one more thing. <b>You cannot see the spread of the votes before
				the game starts</b>. Continuing with our example from above, you would be able to see that BUF @ PIT has 100 
				votes but not how those votes are distributed. In closer games it is common for a lot of people to try
				to take the underdog, making them worth less and possibly making the smarter pick worth more than
				the underdog pick.
			</p>

			<h4>Okay, I think I'm getting it. But why do it at all?</h4>

			<p>
				A few reasons. First it gives you a vested interest in games that you typically wouldn't
				be as interested in. Also, there's the fame and fortune. We declare <b>a champion each
				week</b> based on a running point total for that week (ending after Sunday's games). This
				will ensure that tens of people are aware of your brilliant hockey brain. We are also currently offering 
				<b>weekly prizes</b> to the best of the best.
			</p>

			<h4>Alright it sounds pretty cool. <a href="<?php echo site_url('auth/register')?>">Sign me up!</a></h4>

		</div>

	</div>
		
</div>

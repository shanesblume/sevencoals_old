
<div class='col-md-12' id="news-container">

	<div class="row">

		<div class="col-md-12" id="news-update">

			<h2>Weekly Prizes Are Here!<br><span style="font-size:.6em">...and yes, it's still free.</span></h2>

			<p>
				 NHL hockey is coming back soon, and SevenCoals wants to come back with a bang. So <b>we are offering an Amazon
				 Gift Card to our weekly champions for the rest of the season.</b> This week's prize will be a $10 electronic 
				 gift card, future amounts may vary (in a good way). Make sure you make your picks before <b>February 25th</b> 
				 so you don't miss out! Check the <a href="<?php echo site_url('pages/rules') ?>"> Rules Page </a> for more information. 
			 </p>
			 <p>
				 As a side note, we are doing this in an attempt to grow the site and reward our users with a more competitive and social 
				 experience. <b>Please tell your friends, family, enemies, whoever.</b> We know that more people means more competition, but we are 
				 also hoping to offer more (and better) rewards in the future. And we know with a little help from everyone we can make it happen.
				 Thank You!
			 </p>

		</div>

	</div>

	<hr>

	<div class="row">

		<div class="col-md-12" id="news-update">

			<h2>New Picks Available on Sundays</h2>

			<p>
				Wether you're a weekend warrior or just trying to squeeze as much hockey into the weekend as you can you probably
				don't want to have to make next week's picks on Monday afternoon. So we've added the ability to <b>make next week's
				picks</b> starting first thing Sunday morning. Just go to "My Picks" and click on the <b>"Next Week's Picks"</b> link at the 
				top of the page (available only on Sundays). For scoring purposes weeks will still be starting on Monday. We're just 
				granting everyone VIP early access.
			</p>

		</div>

	</div>

	<hr>

	<div class="row">

		<div class="col-md-12" id="news-update">

			<h2>Twitter Users, Listen Up!</h2>
			
			<p>
				If you're an active Twitter user be sure to <b>follow @sevencoals</b>. We'll be announcing impressive picks, site updates, and stats that may
				help you make smarter choices in the game and in life. <b>Search for us on Twitter or use the link</b> on
				this site. We could use any support we can get! #YOLO #AmIDoingThisRight?
			</p>

		</div>

	</div>
		
</div>



















<div class="text-center" id="getting-started">

	<div class="row">
	<h4>Step 4: Standings</h4>
	<p>If you'd like to see how you are stacking up against other users select "Leaderboards" in the navigation menu.</p>
	<img src="http://sevencoals.com/assets/img/gs_header3.png" style="width:500px;">
	<p>Here you can view your standings for the current week, last weeks results, and the running season-long standings.</p>
	<img src="http://sevencoals.com/assets/img/gs_standings.png" style="width:500px;">

	<h4>Step 5: User Stats</h4>
	<p>Selecting the "User Stats" option in the navigation menu will display data relating to your picks. It'll tell you how accurate you've been, 
	how risky your picks are (voting with or against the majority), and what kind of teams you tend to choose. The dropdown menu will also let you select other users to view their statistics.</p>
	<img src="http://sevencoals.com/assets/img/gs_stats.png" style="width:500px;">
	</div>

</div>
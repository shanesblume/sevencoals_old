
<div class="col-md-12 text-center">

<h2 style="color:#DC8124">This Week's Standings</h2>
<hr>
<h4>1st Place: Everyone Else</h4>
<h4>Last Place: <?php echo $this->tank_auth->get_username(); ?></h4>
<hr>
<div id="page-note">
<p>On a serious note, we apologize for the long load times on the Standings page. The code it was created with hasn't been 
optimized for this many users. It shouldn't take any more than 15-20 seconds to load and this will be fixed very soon. Consider this a testament to how awesome 
our members are (we're looking at you <?php echo $this->tank_auth->get_username() ?>) for spreading the word and helping SevenCoals grow!
</p>

<p>For the actual standings use the link on the left (in the SevenCoals Quick Links section). Or click <a href="<?php echo site_url('games/current_weeks_standings') ?>">here</a>.
You can trust us this time. Promise!
</p>

</div>

</div>

       <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<?php if(!$this->tank_auth->is_logged_in()) { ?>
        		<a href="<?php echo base_url('auth/register') ?>" class="btn btn-primary btn-lg">Start Playing Today <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
       		<?php } else { ?>
        		<a href="<?php echo base_url('games/picks') ?>" class="btn btn-primary btn-lg">Go To My Picks <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        	<?php } ?>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container" id="content">

        <div class="row">

            <div class="col-md-8">
            	        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header" style="background:#8F4E0C;color:#FFFFFF;padding:.5em;">Avalanche Crash Lidstrom's Retirement Party
                    <small>March 06 2014 by Nic Kanaar</small>
                </h1>
            </div>

        </div>
				<p>
				<div class="col-md-4 pull-right">
					<img class="img-responsive" src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Nicklas_Lidstrom_2010-01-31.JPG/613px-Nicklas_Lidstrom_2010-01-31.JPG">
				</div>







<p>
                Let’s start this article off like every other hockey journalist has over the past seventeen years whenever the
                Detroit Red Wings and the Colorado Avalanche have met; by reminding you of the classic brawl of 1998. It was spectacular
                and might have been (and according to most it still is) one of hockey’s greatest rivalries.  With that being said, however, don’t
                expect to see an all-out face bashing contest on March 5th in Detroit. The animosity isn’t as intense as it used to
                be, but don’t worry, there are still plenty of reasons why you should tune in to tonight’s game. SevenCoals
                explains why this game is more exciting than your prom date was.
</p>
 
<p>
                First of all, holy 40 wins Colorado! This team is hotter than a sweaty Kate Upton diving into a pool full
                of freshly nuked Hot Pockets during an August afternoon in southern Texas. With a record of 40-17-05, the
                Avalanche have won 13 of their last 18 games. With stars like Nathan MacKinnon breaking rookie records and
                goaltender Semyon Varlamov playing well, this team is looking unstoppable. So, who could possibly melt this
                aggressive avalanche before it levels a ski lodge? The Red Wings might just be the best bet.
</p>
 
<p>
                In their last four meetings, Detroit has dominated Colorado. Despite how good the Avalanche seem to be,
                the Red Wings tend to always come out on top. Not to mention that tonight’s game will be played at Joe Louis arena with an
                excited, and slightly nervous, crowd. The excitement will be fueled by having some of hockey’s biggest names in
                attendance to help retire Nick Lidstrom’s jersey. Yet the many injuries that currently plague
                Detroit have left them with a slight limp. But there’s hope. With Zetterberg, Weiss, and Datsyuk all out with injuries,
                the Wings picked up veteran center David Legand (Nashville Predators) to help strengthen the offense. Legwand will
                skate with the Red Wings Thursday morning, so it all depends on how fast he can acclimate with his new team.
</p>
 
                The Colorado Avalanche are on a franchise record breaking season, but the Detroit Red Wings are hard to beat with
                their backs against the wall.
                Taking a look at the facts and trying to listen to your gut will cause a lot of inner
                turmoil for everyone making a pick. With both teams needing a strong regular season finish to find post-season
                action, you can bet your pale ass that this will be a spectacular game.
</p>















			        </div>

            <div class="col-md-4">
                <h3 style="background:#8F1E0C;color:#FFFFFF;padding:.5em;">Update: Same Site, New Look<br><small>Mar 05 2014</small></h3>
					<p>
						Welcome to the new SevenCoals! We've spent weeks working this new design and hope everyone enjoys it! While it has delayed 
						some of our other projects (group creation, optional email alerts, etc) we made it a priority after reviewing how our users
						were visiting the site. This version should be much more mobile-friendly for those of you on the go and offer easier access 
						to the functions and information you use the most. There are still a few rough edges we will be cleaning up but we couldn't wait
						 to show off the new design. If you have any questions or concerns be sure to use the feedback form on the 
						home page email or us at admin@sevencoals.com.
					</p>   

				<h3 style="background:#096A26;color:#FFFFFF;padding:.5em;">Prize Winner: djyang27<br><small>Mar 03 2014</small></h3>
					<p>
						Our first weekly prize winner is in the books, and congratulations to djyang27! You have a $10 Amazon.com gift card 
						on the way to your inbox. Thank you everyone for playing last week and we wish you all the best of luck this week. 
						We are going to continue with the $10 gift card as a prize to this weeks champion. However, we are considering the 
						possibility of rewarding more positions as time goes on and as our numbers grow.
					</p>    

				<h3 style="background:#094759;color:#FFFFFF;padding:.5em;">In The Works: More Sports!<br><small>Mar 05 2014</small></h3>
					<p>
						It's hard to admit it but the NHL season is nearing an end. Don't worry! We will definitely be up and running through 
						playoffs, but we aren't stopping there. We are also hoping to continue with a World Cup pick'em in June and, fingers crossed,
						a possible March Madeness pick'em. Stay tuned for more updates!
					</p>           
            </div>

        </div>

	</div>	



 
 
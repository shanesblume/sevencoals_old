
<!-- Show errors on submit -->
<?php echo validation_errors(); ?>

<!-- Panel -->
<div class="panel panel-default col-md-12">

	<!-- Panel Heading -->
	<div class="panel-heading text-center">Create a Game</div>

	<!-- Open Form -->
	<?php echo form_open('games/create_games');

		/* Dropdown Team Options */
		$teamOptions = array(
			'ANA' => 'ANA',
			'BOS' => 'BOS',
			'BUF' => 'BUF',
			'CGY' => 'CGY',
			'CAR' => 'CAR',
			'CHI' => 'CHI',
			'COL' => 'COL',
			'CBJ' => 'CBJ',
			'DAL' => 'DAL',
			'DET' => 'DET',
			'EDM' => 'EDM',
			'FLA' => 'FLA',
			'LAK' => 'LAK',
			'MIN' => 'MIN',
			'MTL' => 'MTL',
			'NSH' => 'NSH',
			'NJD' => 'NJD',
			'NYI' => 'NYI',
			'NYR' => 'NYR',
			'OTT' => 'OTT',
			'PHI' => 'PHI',
			'PHX' => 'PHX',
			'PIT' => 'PIT',
			'SJS' => 'SJS',
			'STL' => 'STL',
			'TBL' => 'TBL',
			'TOR' => 'TOR',
			'VAN' => 'VAN',
			'WAS' => 'WAS',
			'WPG' => 'WPG'
		);

		/* Dropdown Status Options */
		$statusOptions = array(
			'open' => 'Open',
			'unavailable' => 'Unavailable',
			'closed' => 'Closed',
		);
	?>

	<!-- Table -->
	<table class='col-md-12 table'>
		<tr>
			<td><input type='date' name='date'></td>
			<td class='col-md-2'><?php echo form_dropdown('awayTeam', $teamOptions) ?></td>
			<td class='col-md-2'>@</td>
			<td class='col-md-2'><?php echo form_dropdown('homeTeam', $teamOptions) ?></td>
			<td><input type='time' name='time'></td>
			<td class='col-md-2'><?php echo form_dropdown('status', $statusOptions) ?></td>
			<td class='col-md-2'><?php echo form_submit('submit', 'Create Game') ?></td>
		</tr>
	</table>

	<!--Close Form -->
	<?php echo form_close(); ?>

</div> <!-- End Panel -->
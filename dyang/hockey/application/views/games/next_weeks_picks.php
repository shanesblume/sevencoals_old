
<?php 



	$days_list = array();

	foreach($days as $day_info)
	{
		if(!in_array($day_info['date'], $days_list) &&  date('W',strtotime($day_info['date'])) === date('W', strtotime("+1 week")))
		{
			array_push($days_list, $day_info['date']);
		}
	}

	for($i=0;$i<count($days_list);$i++)
	{
		$number_of_games = 0;
		foreach($days as $day_info)
		{
			if($day_info['date'] === $days_list[$i])
			{
				$number_of_games = $number_of_games + 1;
			}

		}
		$days_list[$i] = array($days_list[$i] => $number_of_games);
	}

	$pick_dates_list = array();

	foreach($picks as $pick_info)
	{
		foreach($games as $game_info)
		{
			if($game_info['id'] === $pick_info['game_id'])
			{
				$pick_date = $game_info['date'];
				array_push($pick_dates_list, $pick_date);
			}
		}
	}

	foreach($games as $game_info)
	{
		if($game_info['date'] < date('Y-m-d'))
		{
			$submit_message = 'View';
		}
		else
		{
			$submit_message = 'View';
		}
	}

?>



    <div id="picks-header" class="jumbotron">
      <div class="container">
        <h3>
          Weekly Summary: <span style="color:#DC8124">Next Week</span>
        </h3>

	    <row>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #8F1E0C">
	        		<span id="number">1</span> 
	        	</div>
	        		Position
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #8F4E0C">
	        		<span id="number">0</span>
	        	</div>	
	        		Points Earned
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #094759">
	        		<span id="number">0%</span>
	        	</div>
	        		Pick Accuracy
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #096A26">
	        		<span id="number">
	        			<?php 
	        				$games_remaining = 0;
	        				foreach($games as $game_info)
	        				{
	        					if(date('W',strtotime($game_info['date'])) === date('W', strtotime("+1 week")) && $game_info['status'] !== 'completed')
	        					{
	        						$games_remaining = $games_remaining + 1;
	        					}
	        				}
	        				echo $games_remaining;
	        			?>
	        		</span>
	        	</div>
	        		Games Remaining
	        </div>

        </row>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Week View
                    <small>Next Week</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
                <div id="picks-list">

					<table class="col-xs-12 col-sm-12 col-md-12 table">
						<tr class="table-header">
							<td class="col-xs-3 col-sm-3 col-md-4 text-center">Date</td>
							<td class="col-xs-2 col-sm-2 col-md-3 text-center">Status</td>
							<td class="col-xs-5 col-sm-5 col-md-3 text-center">Picks Made</td>
							<td class="col-xs-2 col-sm-2 col-md-2 text-center">Action</td>

						</tr>
					</table>

					<?php 
						foreach($days_list as $day) 
						{
							foreach($day as $games_in_day) 
							{
								$picks_in_day = count(array_keys($pick_dates_list, key($day)));
								if($games_in_day === $picks_in_day) 
									{
										$day_style = "finished";
									}
								if($games_in_day > $picks_in_day) 
									{
										$day_style = "unfinished";
									}
								if(key($day) < date('Y-m-d'))
									{
										$submit_message = 'View';
										$glyphicon = "<span class='glyphicon glyphicon-check'></span>";
										$status_message = "<span style='font-size:.6em;'>Completed</span>";
									}
								if(key($day) >= date('Y-m-d'))
									{
										$submit_message = 'View';
										$glyphicon = "<span class='glyphicon glyphicon-pencil'></span>";
										$status_message = "<span style='font-size:.6em;'>Open</span>";
									} 
								if(key($day) === date('Y-m-d'))
									{
										$submit_message = 'View';
										$glyphicon = "<span class='glyphicon glyphicon-chevron-right'></span> ";
										$status_message = "<span style='font-size:.6em;'>Current</span>";
									}
							?>

							<table class="col-xs-12 col-sm-12 col-md-12 table">
								<tr>
									<td class="col-xs-3 col-sm-3 col-md-4 text-center" id="picks-date"><?php echo date("D M d", strtotime(key($day))) ?></td>
									<td class="col-xs-2 col-sm-2 col-md-3 text-center"><?php echo $glyphicon.'<br>'.$status_message ?></td>
									<td class="col-xs-5 col-sm-5 col-md-3 text-center" id="<?php echo $day_style.'-picks-numbers'?>"><?php echo $picks_in_day." / ".$games_in_day ?></td>
									<td class="col-xs-2 col-sm-2 col-md-2 text-center"><a href="<?php echo site_url('games/'.key($day)) ?>"><button type="submit" class="btn btn-primary btn-lg" style="width:100%"><?php echo $submit_message ?></button></a></td>

								</tr>
							</table>

						<?php } ?>
					<?php } ?>

				</div>
            </div>

            <div class="col-md-4">
            	<div class="well">
	            	<h3>Key:</h3>
	                <ul>
	                    <li style="background: #EE6D59">Red "Picks Made" Box: You have not made all your picks for the day.</li>
	                    <li style="background: #AAE555">Green "Picks Made" Box: You've made every pick for the day.</li>
	                </ul>
                </div>
                <h3>Notes:</h3>
                <p>Welcome to the Week View. This screen will default to the current week and will always run from Monday to Sunday.
                The following week's picks will open the Sunday before it starts and will be linked below when available. 
                Pressing "View" will take you to a specific day's games, where you can make your picks for that day.</p>

            </div>

        </div>

    </div>
    <!-- /.container -->
    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          Season Stats for <span style="color:#DC8124"><?php echo $username ?></span>
        </h1>

        <div class="col-md-12">

			<?php echo form_open('games/stats');

					/* Dropdown Date Options */
					$user_options = array();
					foreach($users as $user_info) 
					{
						if($user_info['group_id'] === '300') 
						{
							//array_push($user_options, array($user_info['username'] => $user_info['username']));
                                                        $user_options[$user_info['username']] = $user_info['username'];
						}
					}
				?>
				<h3 class="text-center">Select a different user: <?php echo form_dropdown('user_selected', $user_options, $username) ?><br>
				<button type="submit" class="btn btn-primary" style="margin-top:20px;">Show User</button></h3>
				

			<?php echo form_close(); ?>

		</div>

      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container rlogo-background" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">User Statistics
                    <small><?php echo $username ?></small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">

                <div id="stats">

					<div class="row">



					<?php 
						$has_made_picks = FALSE;

						foreach($picks as $pick_info)
						{
							if($username === $pick_info['username'])
							{
								$has_made_picks = TRUE;
							}
						}
					?>

					<?php 
						if($has_made_picks === TRUE)
						{
					?>


					<div class="col-md-6">

						<?php 
							$total_pts = 0;
							$missed_pts = 0;

							foreach($picks as $pick_info) 
							{
								if($pick_info['result'] === '2')
								{
									$total_pts = $total_pts + $pick_info['pts_earned'];
								}

								if($pick_info['result'] === '1')
								{
									foreach($games as $game_info)
									{
										if($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'away')
										{
											$missed_pts = $missed_pts + $game_info['homeVotes'];
										}
										elseif($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'home')
										{
											$missed_pts = $missed_pts + $game_info['awayVotes'];
										}
									}
								}
							} 

							$pts_possible = $total_pts + $missed_pts;
							$pts_percent = round(($total_pts / ($total_pts + $missed_pts))*100);

						?>

						<h4 class="text-center">Points</h4>

						<table class="table">
							<tr>
								<td>Points Earned:</td> 
								<td><?php echo $total_pts ?></td>
							</tr>
							<tr>
								<td>Points Possible:</td> 
								<td><?php echo $pts_possible ?></td>
							</tr>
							<tr>
								<td>Points Percentage:</td> 
								<td><?php echo $pts_percent ?>%</td>
							</tr>
						</table>

					</div>

					<div class="col-md-6">

						<?php 
							$correct_picks = 0;
							$missed_picks = 0;

							foreach($picks as $pick_info) 
							{
								if($pick_info['result'] === '2')
								{
									$correct_picks = $correct_picks + 1;
								}

								if($pick_info['result'] === '1')
								{
									$missed_picks = $missed_picks + 1;
								}
							} 

							$total_picks = $correct_picks + $missed_picks;
							$picks_percent = round(($correct_picks / $total_picks)*100);

						?>

						<h4 class="text-center">Picks</h4>

						<table class="table">
							<tr>
								<td>Correct Picks:</td> 
								<td><?php echo $correct_picks ?></td>
							</tr>
							<tr>
								<td>Total Picks:</td> 
								<td><?php echo $total_picks ?></td>
							</tr>
							<tr>
								<td>Pick Accuracy:</td> 
								<td><?php echo $picks_percent ?>%</td>
							</tr>
						</table>

					</div>

					<div class="col-md-6">
						<?php 
							$majority_picks = 0;
							$minority_picks = 0;

							foreach($picks as $pick_info)
							{
								foreach($games as $game_info)
								{
									if($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'home')
									{
										if($game_info['homeVotes'] > $game_info['awayVotes'])
										{
											$majority_picks = $majority_picks +1;
										}
										elseif($game_info['homeVotes'] < $game_info['awayVotes'])
										{
											$minority_picks = $minority_picks +1;
										}
									}
									elseif($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'away')
									{
										if($game_info['homeVotes'] > $game_info['awayVotes'])
										{
											$minority_picks = $minority_picks +1;
										}
										elseif($game_info['homeVotes'] < $game_info['awayVotes'])
										{
											$majority_picks = $majority_picks +1;
										}
									}

								}
							}

							$safe_picks_percent = round(($majority_picks / ($majority_picks + $minority_picks))*100);
							$risky_picks_percent = round(($minority_picks / ($majority_picks + $minority_picks))*100);
						?>

						<h4 class="text-center">Trends</h4>

						<table class="table">
							<tr>
								<td>Safe Picks:</td> 
								<td><?php echo $safe_picks_percent ?>%</td>
							</tr>
							<tr>
								<td>Risky Picks:</td> 
								<td><?php echo $risky_picks_percent ?>%</td>
							</tr>
						</table>

					</div>

					<div class="col-md-6">
						<?php 

							$teams_picked = array();

							foreach($picks as $pick_info) 
							{
								foreach($games as $game_info)
								{
									if($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'home')
									{
										array_push($teams_picked, $game_info['homeTeam']);
									}
									elseif($pick_info['game_id'] === $game_info['id'] && $pick_info['pick'] === 'away')
									{
										array_push($teams_picked, $game_info['awayTeam']);
									}
								}
							}

							$teams_picked_count = array_count_values($teams_picked);
							$teams_picked_max = array_search(max($teams_picked_count), $teams_picked_count);
							$teams_picked_min = array_search(min($teams_picked_count), $teams_picked_count);

						?>

						<h4 class="text-center">Favorites</h4>

						<table class="table">
							<tr>
								<td>Most Common Pick:</td> 
								<td><?php echo $teams_picked_max ?></td>
							</tr>
							<tr>
								<td>Most Avoided Pick:</td> 
								<td><?php echo $teams_picked_min ?></td>
							</tr>
						</table>

					</div>

					<?php } ?>

					</div>
				</div>
            </div>

            <div class="col-md-4">
                <h3>Note</h3>
				<p>
					All stats are updated as games finish and we will be adding much more to this section in the coming weeks. Use it to reanalyze your strategy 
					or research what other players are doing different. And we promise to never give away enough information for another user to figure out exactly
					what your going to do.
				</p>
            </div>

        </div>

    </div>
    <!-- /.container -->
















       <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container" id="content">

        <div class="row">

            <div class="col-md-8">
            	 
            	 <div class="row">

		            <div class="col-lg-12">
		                <h1 class="page-header" style="background:#8F4E0C;color:#FFFFFF;padding:.5em;">Update Results
		                    <small>SevenCoals Admin</small>
		                </h1>
		            </div>

		            <?php $initialDate = FALSE; ?>
<?php $gameNumber = 0; ?>
<?php $numberOfGames = 0; ?>

<?php echo form_open('games/update_results'); ?>
	<button type="submit" class="col-md-2 col-md-offset-7 btn btn-picks" style="margin-bottom: 20px;">Submit Results</button>

	<div class='panel panel-default col-md-12' id='games-list'>
		<table class='table'>

			<?php foreach ($games as $game_info): ?>

				<?php if($game_info['status'] === 'closed') { ?>

					<?php 
						$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
						$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
						$currentDate = $game_info['date'];	
						$game_id = $game_info['id'];
						$gameNumber = $gameNumber + 1;
						$numberOfGames = $numberOfGames + 1;

						echo form_hidden('game_id_'.$gameNumber, $game_info['id']);		
						echo form_hidden('number_of_games', $numberOfGames);		

						if($initialDate === FALSE) {
							$previousDate = $currentDate; 
							echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
						}
					?>

					<?php if($currentDate !== $previousDate) {
						echo "</table>";
						echo "</div>";
						echo "<div class='panel panel-default col-md-6 col-md-offset-3' id='games-list'>";
						echo "<table class='table'>";
						echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
					} ?>

					<tr>
						<td class="col-md-2"><?php echo date("g:iA", strtotime($game_info['time'])) ?></td>
						<td class='col-md-3 text-center' id=''>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
							</div>
							<input type="radio" name="result_<?php echo $gameNumber ?>" value="away" >
							<?php echo $game_info['awayTeam'] ?>
						</td> 
						<td class='col-md-1 text-center'><br>@</td>
						<td class='col-md-3 text-center' id=''>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
							</div>
							<input type="radio" name="result_<?php echo $gameNumber ?>" value="home" >
							<?php echo $game_info['homeTeam'] ?>
						</td>

					</tr>

					<?php $previousDate = $currentDate; ?>
					<?php $initialDate = TRUE; ?>

				<?php } ?>

			<?php endforeach ?>
		</table>

	</div>

	<button type="submit" class="col-md-2 col-md-offset-7 btn btn-primary">Submit Results</button>
<?php echo form_close() ?>

        		  </div>
          
            </div>

        </div>

	</div>	




<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
?>

    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<a href="<?php echo base_url('auth/login') ?>" class="btn btn-primary btn-lg">Already a Member? <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->


    <div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">It's Easy to Register!
                    <small>Use the Form Below</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
                <?php echo form_open($this->uri->uri_string()); ?>

					<div class="col-md-12" id="login-page">
							<table class="table text-left" style="border:none">
								<?php if ($use_username) { ?>
								<tr>
									<td class="col-md-4"><?php echo form_label('Username', $username['id']); ?></td>
									<td class="col-md-4"><?php echo form_input($username); ?></td>
									<td class="col-md-4" style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></td>
								</tr>
								<?php } ?>
								<tr>
									<td class="col-md-4"><?php echo form_label('Email Address', $email['id']); ?></td>
									<td class="col-md-4"><?php echo form_input($email); ?></td>
									<td class="col-md-4" style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
								</tr>
								<tr>
									<td class="col-md-4"><?php echo form_label('Password', $password['id']); ?></td>
									<td class="col-md-4"><?php echo form_password($password); ?></td>
									<td class="col-md-4" style="color: red;"><?php echo form_error($password['name']); ?></td>
								</tr>
								<tr>
									<td class="col-md-4"><?php echo form_label('Confirm Password', $confirm_password['id']); ?></td>
									<td class="col-md-4"><?php echo form_password($confirm_password); ?></td>
									<td  class="col-md-4" style="color: red;"><?php echo form_error($confirm_password['name']); ?></td>
								</tr>
								<tr>
									<td class="col-md-4"></td>
									<td  class="col-md-4"><button type="submit" class="btn btn-primary" style="width:100%">Login</button></td>
									<td  class="col-md-4"></td>
								</tr>
							</table>
					</div>
					<?php echo form_close(); ?>
            </div>

            <div class="col-md-4">
                <h3>It's Free, Fast, and Secure</h3>
					<p>
						At SevenCoals we care about your privacy and security. We will not share your personal information. We also promise to
						ensure that this site is safe for you to use, starting with SiteLock monitoring and a tried-and-true authentication method.
						If you have any questions or concerns please feel free to contact us at admin@sevencoals.com
					</p>                
            </div>

        </div>

	</div>





<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'pages/view';
$route['pages/(:any)'] = 'pages/view/$1';
$route['games/user/(:any)'] = 'games/user/$1';
$route['games/(:any)'] = 'games/view/$1';
$route['contact'] = 'pages/contact';
$route['games/create_games'] = 'games/create_games';
$route['games/update_results'] = 'games/update_results';
$route['games/next_weeks_picks'] = 'games/next_weeks_picks';
$route['games/results'] = 'games/results';
$route['games/current'] = 'games/current';
$route['games/standings'] = 'games/standings';
$route['games/last_weeks_standings'] = 'games/last_weeks_standings';
$route['games/current_weeks_standings'] = 'games/current_weeks_standings';
$route['games/season_standings'] = 'games/season_standings';
$route['games/picks'] = 'games/picks';
$route['games/stats'] = 'games/stats';
$route['games/view_day'] = 'games/view_day';
$route['games'] = 'games';
$route['confirm_sent'] = 'email/confirm_sent';
$route['error_sent'] = 'email/error_sent';
$route['games/index'] = 'games/index';
$route['404_override'] = 'errors/page_missing';



/* End of file routes.php */
/* Location: ./application/config/routes.php */
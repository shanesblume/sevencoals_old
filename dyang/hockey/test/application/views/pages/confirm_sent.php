<div class="well col-md-12">

	<div class='alert alert-success text-center' id='contact-messages'>

		<h4>Your message has been sent!</h4>
		<p>If needed you will receive a response from us as soon as possible.<p>
		<p>Thank you for your support and we appreciate your input!<p>

	</div>

</div>
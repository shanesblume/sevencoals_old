<div class="well col-md-12">

	<div class='alert alert-danger text-center' id='contact-messages'>

		<h4>Oh no, your message failed!</h4>
		<p>It looks like you may have left the text box empty.<p>
		<p>Please try again, we really want to hear what you have to say!<p>
		<p>We appreciate and value your input, sorry for the inconvenience.<p>

	</div>

</div>
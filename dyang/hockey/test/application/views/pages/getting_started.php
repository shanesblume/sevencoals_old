
<div class="text-center" id="getting-started" style="padding-left: 20px; padding-right:20px;">

	<div class="row" id="page-note">
	<p>Note: The screenshots used  below are from a previous design we had. The pictures may not be the same as what
	 you see on your screen, but the process is still the same!</p>
	</div>

	<div class="row">
	<h4>Step 1: Register</h4>
	<p>Before making your picks we suggest you read the Introduction and Rules pages 
	to understand how the game works. When you're ready to get started click on "Register" 
	at the top of the page. If you've already created a profile, click "Login".</p>
	<img src="http://sevencoals.com/assets/img/gs_header1.png" style="width:500px;">
	<p>To create an account enter your desired username and an email address. We do not share
	 your email or other information with anyone. Be sure to check your inbox for an email 
	 from us after your account is completed. You do not have to verify your email address but
	 if any prizes are won this is how we will contact you and confirm delivery information.</p>
	<img src="http://sevencoals.com/assets/img/gs_login.png" style="width:500px;">
	<br><br><a href="http://sevencoals.com/pages/getting_started_2">Go To Step 2</a>
	</div>

</div>

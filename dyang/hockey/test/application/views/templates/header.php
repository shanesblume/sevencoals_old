
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="An NHL hockey pick'em game with a unique scoring system. It's a free, competitive way to prove your hockey knowledge.">

    <title>SevenCoals | A New Kind of Hockey Pick'em</title>
    
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://test.sevencoals.com/assets/css/test_style.css">
    <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
    <link href="sevencoals.com/assets/img/favicon.ico" rel="shortcut icon" />

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-47684602-1', 'sevencoals.com');
      ga('send', 'pageview');
    </script>

  </head>

  <body>

  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="/" class="navbar-brand">SevenCoals</a>
      </div>
      <nav class="collapse navbar-collapse" role="navigation">
        <ul class="nav navbar-nav">
          <li>
            <a href="<?php echo base_url('pages/Blog')?>">News & Updates</a>
          </li>
          <li>
            <a href="<?php echo base_url('games/picks')?>">My Picks</a>
          </li>
          <li>
            <a href="<?php echo base_url('games/standings')?>">Standings</a>
          </li>
          <li>
            <a href="<?php echo base_url('games/stats')?>">User Stats</a>
          </li>
          <li>
            <li class="dropdown">
              <a href="<?php echo base_url('') ?>" class="dropdown-toggle" data-toggle="dropdown">Learn More <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('') ?>">Welcome to SevenCoals</a></li>
                <li><a href="<?php echo base_url('') ?>#introduction">An Introduction</a></li>
                <li><a href="<?php echo base_url('') ?>#faq">FAQ</a></li>
                <li><a href="<?php echo base_url('') ?>#rules">Rules</a></li>
                <li><a href="<?php echo base_url('') ?>#getting-started">Getting Started</a></li>
                <li><a href="<?php echo base_url('') ?>#feedback">Feedback</a></li>
              </ul>
            </li>
            <?php if(!$this->tank_auth->is_logged_in()) { ?>
              <li><a href="<?php echo base_url('auth/login')?>">Sign In</a></li>
              <li><a href="<?php echo base_url('auth/login')?>">Register</a></li>
            <?php } else { ?>
              <li>
                <li class="dropdown">
                  <a href="<?php echo base_url('') ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->tank_auth->get_username() ?><b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url('auth/logout') ?>">Logout</a></li>
                    <li><a href="<?php echo base_url('auth/change_email') ?>">Change Email</a></li>
                  </ul>
                </li>
              </li>
            <?php } ?>
          </li>
        </ul>
      </nav>
    </div>
  </nav>

    

<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>

    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<a href="<?php echo base_url('games/picks') ?>" class="btn btn-primary btn-lg">Go to My Picks <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->


        <div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Change My Email
                    <small>Use the Form Below</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
				<?php echo form_open($this->uri->uri_string()); ?>
				<table>
					<tr>
						<td><?php echo form_label('Password', $password['id']); ?></td>
						<td><?php echo form_password($password); ?></td>
						<td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
					</tr>
					<tr>
						<td><?php echo form_label('New email address', $email['id']); ?></td>
						<td><?php echo form_input($email); ?></td>
						<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
					</tr>
				</table>
				<?php echo form_submit('change', 'Send confirmation email'); ?>
				<?php echo form_close(); ?>
            </div>

            <div class="col-md-4">
                <h3>It's Free, Fast, and Secure</h3>
					<p>
						At SevenCoals we care about your privacy and security. We will not share your personal information. We also promise to
						ensure that this site is safe for you to use, starting with SiteLock monitoring and a tried-and-true authentication method.
						If you have any questions or concerns please feel free to contact us at admin@sevencoals.com
					</p>                
            </div>

        </div>

</div>




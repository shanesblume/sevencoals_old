
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Username';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>

    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<a href="<?php echo base_url('auth/register') ?>" class="btn btn-primary btn-lg">Need to Register? <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->


    <div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Already a Member?
                    <small>Login Below</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
                <?php echo form_open($this->uri->uri_string()); ?>

					<div class="col-md-12" id="login-page">
						<table class="table text-center" style="border:none">
							<tr>
								<td class="col-md-4"><?php echo form_label($login_label, $login['id']); ?></td>
								<td class="col-md-4"><?php echo form_input($login); ?></td>
								<td class="col-md-4" style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
							</tr>
							<tr>
								<td class="col-md-4"><?php echo form_label('Password', $password['id']); ?></td>
								<td class="col-md-4"><?php echo form_password($password); ?></td>
								<td  class="col-md-4" style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
							</tr>

							<tr>
								<td  class="col-md-4"><?php echo anchor('/auth/forgot_password/', 'I forgot my password'); ?></td>
								<td  class="col-md-4">
									<?php echo form_checkbox($remember); ?>
									<?php echo form_label('Remember me', $remember['id']); ?>
								</td>
								<td class="col-md-4"></td>
							</tr>

							<tr>
								<td class="col-md-4"></td>
								<td  class="col-md-4"><button type="submit" class="btn btn-primary" style="width:100%">Login</button></td>
								<td  class="col-md-4"></td>
							</tr>
						</table>
					</div>
				<?php echo form_close(); ?>
            </div>

            <div class="col-md-4">
                <h3>It's Free, Fast, and Secure</h3>
					<p>
						At SevenCoals we care about your privacy and security. We will not share your personal information. We also promise to
						ensure that this site is safe for you to use, starting with SiteLock monitoring and a tried-and-true authentication method.
						If you have any questions or concerns please feel free to contact us at admin@sevencoals.com
					</p>                
            </div>

        </div>

	</div>	

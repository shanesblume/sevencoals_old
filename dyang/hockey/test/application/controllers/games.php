<?php

class Games extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('games_model');
	}

	public function index()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}
		$this->games_model->update_game_status();

		$data['title'] = 'Picks';
		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();

		$this->form_validation->set_rules('date_selected', 'Date', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('games/index');
			$this->load->view('templates/footer');

		}
		else
		{
			$date_slug = $this->input->post('date_selected');
			redirect('games/'.$date_slug);
		}
	}


	public function picks()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}

		$this->games_model->update_game_status();

		$data['title'] = 'Week View';
		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['days'] = $this->games_model->get_week();
		$data['leaderboard_title'] = 'Weekly';


		$this->load->view('templates/header', $data);
		$this->load->view('games/picks');
		$this->load->view('templates/footer');
	}

		public function next_weeks_picks()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}

		$this->games_model->update_game_status();

		$data['title'] = 'Next Week';
		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['days'] = $this->games_model->get_week();
		$data['leaderboard_title'] = 'Weekly';

		if(date('l') !== 'Sunday') { redirect('games/picks'); }

		$this->load->view('templates/header', $data);
		$this->load->view('games/next_weeks_picks');
		$this->load->view('templates/footer');
	}

	public function view($date_slug)
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}

		if(date('W',strtotime($date_slug)) > date('W', strtotime("+1 week")))
			{
				redirect('games/picks');
			}
		if(date('W',strtotime($date_slug)) === date('W', strtotime("+1 week")) && date('l') !== 'Sunday')
			{
				redirect('games/picks');
			}

		$data['title'] = 'Day View';
		$data['games'] = $this->games_model->get_games($date_slug);
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['leaderboard_title'] = 'Daily';

		$this->form_validation->set_rules('username', 'Username', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('games/view', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->games_model->set_picks();
			redirect('games/picks');
		}
	}

	public function results()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}
		$this->games_model->update_game_status();

		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['results'] = $this->games_model->get_results();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['title'] = 'Results';
		$data['leaderboard_title'] = 'Weekly';

		$this->load->view('templates/header', $data);
		$this->load->view('games/results');
		$this->load->view('templates/footer');

	}

	public function current()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}
		$this->games_model->update_game_status();

		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['standings'] = $this->games_model->get_standings();
		$data['results'] = $this->games_model->get_results();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['title'] = 'Current';

		$this->load->view('templates/header', $data);
		$this->load->view('games/current');
		$this->load->view('templates/footer');
		$data['leaderboard_title'] = 'Weekly';

	}

	public function update_results()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}

		if(!$this->tank_auth->is_admin()) {
			redirect('');
		}
		
		$this->games_model->update_game_status();

		$data['title'] = 'Update Results';
		$data['games'] = $this->games_model->get_games();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['leaderboard_title'] = 'Weekly';
		
		$this->games_model->update_results();
		$this->load->view('templates/header', $data);
		$this->load->view('games/update_results');
		$this->load->view('templates/footer');	
	}

	public function create_games()
	{
		if(!$this->tank_auth->is_logged_in()) {
			redirect('auth/login');
		}

		if(!$this->tank_auth->is_admin()) {
			redirect('');
		}

		$this->games_model->update_game_status();

		$data['title'] = 'Create Games';
		$data['standings'] = $this->games_model->get_standings();
		$data['games'] = $this->games_model->get_games();
		$data['notables'] = $this->games_model->get_notable_picks();

		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('time', 'Time', 'required');
		$this->form_validation->set_rules('awayTeam', 'Away Team', 'required');
		$this->form_validation->set_rules('homeTeam', 'Home Team', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('games/create_games');
			$this->load->view('templates/footer');

		}
		else
		{
			$this->games_model->set_games();
			redirect('games/create_games');
		}
	}

	public function stats()
	{

		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['users'] = $this->games_model->get_users();
		$data['standings'] = $this->games_model->get_standings();
		$data['results'] = $this->games_model->get_results();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['title'] = 'Stats';
		$data['leaderboard_title'] = 'Weekly';

		$this->form_validation->set_rules('user_selected', 'User', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('games/stats');
			$this->load->view('templates/footer');

		}
		else
		{
			$user_slug = $this->input->post('user_selected');
			redirect('games/user/'.$user_slug);
		}	
	}

	public function user($user_slug)
	{

		$data['title'] = 'User';
		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_user_picks($user_slug);
		$data['users'] = $this->games_model->get_users();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['username'] = $user_slug;
		$data['leaderboard_title'] = 'Weekly';

		$this->load->view('templates/header', $data);
		$this->load->view('games/user');
		$this->load->view('templates/footer');
	}

	public function standings()
	{

		$data['games'] = $this->games_model->get_games();
		$data['picks'] = $this->games_model->get_picks();
		$data['users'] = $this->games_model->get_users();
		$data['standings'] = $this->games_model->get_standings();
		$data['results'] = $this->games_model->get_results();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['title'] = 'Stats';
		$data['leaderboard_title'] = 'Weekly';

		$this->form_validation->set_rules('week_selected', 'Week', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('games/standings');
			$this->load->view('templates/footer');

		}
		else
		{
			$week_slug = $this->input->post('week_selected');
			redirect('games/standings_archive/'.$week_slug);
		}	
	}

	public function standings_archive($week_slug)
	{

		$data['title'] = 'User';
		$data['games'] = $this->games_model->get_games();
		$data['users'] = $this->games_model->get_users();
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['week'] = $week_slug;
		$data['leaderboard_title'] = 'Weekly';

		$this->load->view('templates/header', $data);
		$this->load->view('games/standings_archive');
		$this->load->view('templates/footer');
	}

}
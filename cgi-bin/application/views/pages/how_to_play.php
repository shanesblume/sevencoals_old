<div class="col-md-12 well text-justified" id="how-to">

<div class="jumbotron text-center">
	<p>It's Hockey Pick'em with a <em>Twist</em>!</p>
	<h2>How To Play SevenCoals Hockey Pick'em</h2>
</div>

<h4>What is a hockey pick'em?</h4>
<p>
	In a hockey picke'em you <b>try to predict who is going to
	win an NHL hockey game</b> based on reasearch, hockey-smarts, a gut-feeling, or
	good ol' reliable luck; whichever strategy you choose. Your picks are matched 
	up against other players to see who does better.
</p>

<h4>Wow, that sounds...bland. And I've seen it before. Make it better.</h4>
<p>
	We did! With SevenCoals Hockey Pick'em <b>every game is worth a variable 
	amount of points</b> depending on how many people voted on a game and wether
	or not you are voting with the minority or the majority. If you correctly
	predict the winner of a game you get <b>one point for each person that picked
	the losing team</b>.
</p>

<h4>Sorry, I had a text. Can you explain that again?</h4>
<p>
	Absolutely. Let's say the Buffalo Sabres are playing the Pittsburgh Penguins. 
	100 people made picks and out of those 100 picks submitted, 80 users picked 
	Pittsburgh as the winner while the other 20 people picked Buffalo.
</p>

<h4>Wait... stop right there. Do the people who picked Buffalo have a death wish?</h4>
<p>
	It's just an example, keep reading. If Pittsburgh wins, anyone who picked 
	them would get 20pts (because 20 people picked Buffalo) and everyone who picked Buffalo 
	would get 0pts (because they were wrong). But if Buffalo wins...
</p>

<h4>Wait... stop again.</h4>
<p>
	IT'S JUST AN EXAMPLE! <b>IF</b> Buffalo wins (stop laughing, it could happen) the 20 people that picked them
	would get 80pts each (beacuse 80 people picked Pittsburgh, whose laughing now?) and the 80 people that 
	picked Pittsburgh would get 0pts each (beacuse they were wrong).
</p>

<h4>Okay, so I should always vote for the underdog?</h4>
<p>
	Not at all. Even though <b>you don't lose any points for an incorrect pick</b>, you don't
	gain any either. You'll have to decide if the reward of more points is worth the 
	risk of getting none at all. Oh, and one more thing. <b>You cannot see the results of the voting before
	the game starts</b>. In our example above you would see that BUF @ PIT has 100 votes but
	not who those votes are for. In closer games it is common for a lot of people to try
	to take the underdog, making them worth less and possibly making the smarter pick worth more than
	the underdog pick.
</p>

<h4>Okay, I think I'm getting it. But why do it at all?</h4>
<p>
	A few reasons. First it gives you a vested interest in games that you typically wouldn't
	be as interested in. Also, there's the fame and fortune. We declare <b>a champion each
	week</b> based on a running point total for that week (ending after Sunday's games). This
	will ensure that tens of people are aware of your brilliant hockey brain. We also plan
	on offering weekly and season-long prizes (eventually). There are still a lot of bugs to 
	work out in that area though, so we will update you with more details.
</p>

<h4>Alright, I'm in. And I want to get my friends to join so the picks are worth more. Will you change the
	name so I don't feel weird talking about it?</h4>
<p>
	Nope. But <b>please do tell your friends!</b>
</p>

<h4>Fair enough. <a href="<?php echo site_url('auth/register')?>">Sign me up!</a></h4>

</div>

<?php $initialDate = FALSE; ?>
<?php $gameNumber = 0; ?>
<?php $numberOfGames = 0; ?>
<?php $todaysDate = date('Y-m-d'); ?>
<?php $todaysTime = date('H:i:s'); ?>

<?php echo form_open('games/view'); ?>

	<div class='panel panel-default col-md-12' id='games-list'>
		<table class='table'>

			<?php foreach ($games as $game_info): ?>

				<?php if( ($game_info['date'] > $todaysDate) || ($game_info['date'] === $todaysDate && $game_info['time'] > $todaysTime) ) { ?>

					<?php 
						$away_logo_url = base_url('/assets/img/team_logos').'/'.strtolower($game_info['awayTeam']).'_logo.png';
						$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
						$currentDate = $game_info['date'];	
						$game_id = $game_info['id'];
						$gameNumber = $gameNumber + 1;
						$numberOfGames = $numberOfGames + 1;
						$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
						$awayPicked = FALSE;
						$homePicked = FALSE;
						$teamPicked = 'team-picked';

						foreach ($picks as $pick_info):
							if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') {
								$awayPicked = TRUE;
							} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') {
								$homePicked = TRUE;
							}
						endforeach;

						echo form_hidden('username', $this->tank_auth->get_username());
						echo form_hidden('game_id_'.$gameNumber, $game_info['id']);		
						echo form_hidden('number_of_games', $numberOfGames);
						echo form_hidden('game_date_'.$gameNumber, date('Y-m-d', strtotime($game_info['date'])));
						echo form_hidden('game_time_'.$gameNumber, date('H:i:s', strtotime($game_info['time'])));

						if($initialDate === FALSE) {
							$previousDate = $currentDate; 
							echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
						}
					?>

					<?php if($currentDate !== $previousDate) {
						echo "</table>";
						echo "</div>";
						echo "<div class='panel panel-default col-md-12' id='games-list'>";
						echo "<table class='table'>";
						echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
					} ?>

					<tr>
						<td class="col-md-2"><?php echo date("g:iA", strtotime($game_info['time'])) ?></td>
						<td class='col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
							<label for="away_<?php echo $gameNumber ?>">
								<div id="logo-wrap">
									<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
								</div>
							</label><br>
							<input id="away_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="away" <?php if($awayPicked === TRUE) {echo "checked";} ?>>
							<?php echo $game_info['awayTeam'] ?>
						</td> 
						<td class='col-md-1 text-center'><br>@</td>
						<td class='col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
							<label for="home_<?php echo $gameNumber ?>">
								<div id="logo-wrap">
									<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
								</div>
							</label><br>
							<input id="home_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="home" <?php if($homePicked === TRUE) {echo "checked";} ?>>
							<?php echo $game_info['homeTeam'] ?>
						</td>
						<td class='col-md-3 text-center'><?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?></td>

					</tr>

					<?php $previousDate = $currentDate; ?>
					<?php $initialDate = TRUE; ?>

				<?php } ?>

			<?php endforeach ?>
		</table>

	</div>

	<button type="submit" class="col-md-3 col-md-offset-9 btn btn-picks">Submit My Picks</button>
<?php echo form_close() ?>
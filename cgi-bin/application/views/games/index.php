
<!-- Show errors on submit -->
<?php echo validation_errors(); ?>

<!-- Panel -->
<div class="panel panel-default col-md-12">

	<!-- Panel Heading -->
	<div class="panel-heading text-center">Select a Date</div>

	<!-- Open Form -->
	<?php echo form_open('games/index');

		/* Dropdown Date Options */
		$dateOptions = array();
		foreach($games as $game_info) {

			if($game_info['status'] === 'open' && !in_array(array($game_info['date'] => date("l M. d, Y", strtotime($game_info['date']))), $dateOptions)) {
				array_push($dateOptions, array($game_info['date'] => date("l M. d, Y", strtotime($game_info['date']))));
			}
		}
	?>

	<!-- Table -->
	<table class='col-md-12 table'>
		<tr>
			<td class="text-right"><br>Select a Date:</td>
			<td class="text-left"><br><?php echo form_dropdown('date_selected', $dateOptions) ?></td>
			<td class="text-left"><button type="submit" class="btn btn-picks">Show Games</button></td>
		</tr>
	</table>

	<!--Close Form -->
	<?php echo form_close(); ?>

</div> <!-- End Panel -->



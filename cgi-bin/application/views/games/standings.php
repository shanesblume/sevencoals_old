<div class="well">
	<p>
		While weekly champions are the real heroes of our pick'em, we also offer a look into your overall standings for the season. 
		Select a time-frame in the tabs below to see how you compare to the competition this week, last week, or over the full season.
	</p>
</div>


<div class="well">

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="#this_week" data-toggle="tab">This Week</a></li>
  <li><a href="#last_week" data-toggle="tab">Last Week</a></li>
  <li><a href="#this_season" data-toggle="tab">This Season</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

 	<div class="tab-pane active" id="this_week">
			<?php $users = array(); ?>
			<?php $points = array(); ?>
			<?php $result = array(); ?>

			<h4 class="text-center">Current Weekly Leaderboard</h4>
			<table class='table table-striped'>

		      <?php foreach($standings as $standings_info) 
		      {
		        foreach($games as $game_info)
		        {
		          if($game_info['id'] === $standings_info['game_id'] && date('W',strtotime($game_info['date'])) === date('W'))
		          {
		            $username = $standings_info['username'];
		            if(!in_array($username, $users)) 
		            {
		               array_push($users, $username);
		            }
		          }
		         
		        }
		      }

	          foreach($users as $user) {

	            $total_pts = 0;

	            foreach($standings as $standings_info) 
	            {
	              foreach($games as $game_info)
	              {
	                if($game_info['id'] === $standings_info['game_id'])
	                {
	                  if($user === $standings_info['username'] && date('W',strtotime($game_info['date'])) === date('W'))
	                  {
	                    $total_pts = $total_pts + $standings_info['pts_earned'];
	                  }
	                }
	              }
	            }
	            array_push($points, $total_pts);
	          }

			for($i=0;$i<count($users);$i++) {
				$result[$users[$i]] = $points[$i];
			}

			arsort($result);
			$position = 1;
			foreach($result as $user_key=>$points_value)
		    {
		    	if($user_key === $this->tank_auth->get_username()) {
			    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
				    		<td class='text-right'>".$position.".</td>
				    		<td>".$user_key."</td><td>".$points_value." points</td>
				    	</tr>";
				} else {
				    echo "<tr>
				    		<td class='text-right'>".$position.".</td>
				    		<td>".$user_key."</td><td>".$points_value." points</td>
				    	</tr>";
			    }
			    $position = $position + 1;
		    }
			?>

			</table>
	</div>

  <div class="tab-pane" id="last_week">
		  		<?php $users = array(); ?>
				<?php $points = array(); ?>
				<?php $result = array(); ?>

				<h4 class="text-center">Last Week's Leaderboard</h4>
				<table class='table table-striped'>

			      <?php foreach($standings as $standings_info) 
			      {
			        foreach($games as $game_info)
			        {
			          if($game_info['id'] === $standings_info['game_id'] && date('W',strtotime($game_info['date'])) === date('W', strtotime("-1 week")))
			          {
			            $username = $standings_info['username'];
			            if(!in_array($username, $users)) 
			            {
			               array_push($users, $username);
			            }
			          }
			         
			        }
			      }

		          foreach($users as $user) {

		            $total_pts = 0;

		            foreach($standings as $standings_info) 
		            {
		              foreach($games as $game_info)
		              {
		                if($game_info['id'] === $standings_info['game_id'])
		                {
		                  if($user === $standings_info['username'] && date('W',strtotime($game_info['date'])) === date('W', strtotime("-1 week")))
		                  {
		                    $total_pts = $total_pts + $standings_info['pts_earned'];
		                  }
		                }
		              }
		            }
		            array_push($points, $total_pts);
		          }

				for($i=0;$i<count($users);$i++) {
					$result[$users[$i]] = $points[$i];
				}

				arsort($result);
				$position = 1;
				foreach($result as $user_key=>$points_value)
			    {
			    	if($user_key === $this->tank_auth->get_username()) {
				    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
					} else {
					    echo "<tr>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
				    }
				    $position = $position + 1;
			    }
				?>

				</table>
	</div>



  <div class="tab-pane" id="this_season">
			<?php $users = array(); ?>
			<?php $points = array(); ?>
			<?php $result = array(); ?>

					<h4 class="text-center">Current Season Leaderboard</h4>
					<table class='table table-striped'>

					<?php foreach($standings as $standings_info) {

						$username = $standings_info['username'];

						if(!in_array($username, $users))
						{
							array_push($users, $username);
						}
					} 

					foreach($users as $user) {

						$total_pts = 0;

						foreach($standings as $standings_info) {
							if($user === $standings_info['username'])
							{
								$total_pts = $total_pts + $standings_info['pts_earned'];
							}
						}

						array_push($points, $total_pts);
					}

					for($i=0;$i<count($users);$i++) {
						$result[$users[$i]] = $points[$i];
					}

					arsort($result);
					$position = 1;
					foreach($result as $user_key=>$points_value)
				    {
				    	if($user_key === $this->tank_auth->get_username()) {
					    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
						    		<td class='text-right'>".$position.".</td>
						    		<td>".$user_key."</td><td>".$points_value." points</td>
						    	</tr>";
						} else {
						    echo "<tr>
						    		<td class='text-right'>".$position.".</td>
						    		<td>".$user_key."</td><td>".$points_value." points</td>
						    	</tr>";
					    }
					    $position = $position + 1;
				    }
					?>

					</table>
  	
  </div>
</div>
</div>



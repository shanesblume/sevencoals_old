<div class="container">

<div id="header" >

  <div class="col-xs-12 col-sm-12 col-md-2 text-center" style="background-color: black;">

    <a href="http://sevencoals.com">
      <div style="background-color:#232B2B;" id="logo"></div>
    </a>

  </div>

  <div class="col-xs-12 col-sm-12 col-md-10" style="background-color:#232B2B;">
    <!-- Header Text -->
    <h3 style="text-align:left; float:left;">SevenCoals Presents: <span style="color:#FFFFFF;">A New Kind of Hockey Pick'em</span></h3>

    <div class="col-md-4" id="header-login">
      <?php if($this->tank_auth->is_logged_in()) { ?>
          <h3 style="text-align:right; float:right;">Signed is as: <span style="color:#FFFFFF"><?php echo $this->tank_auth->get_username() ?></span> | <a href="<?php echo site_url('auth/logout') ?>">Logout</a></h3>
      
      <?php } else { ?>
          <h3 style="text-align:right; float:right;"><a href="<?php echo site_url('auth/login') ?>">Login</a> | <a href="<?php echo site_url('auth/register') ?>">Register</a></h3>
      <?php } ?> 
    </div  

        <!-- Navbar -->
        <div class="navbar navbar-default" style="clear:both;" role="navigation">

          <!--  Navbar Collapse -->
          <div class="navbar-inner row">

            <ul class="col-xs-12 col-sm-12 col-md-8 nav navbar-nav" id="static-nav">
              <li><a href="<?php echo site_url()?>">Home</a></li>
              <li><a href="<?php echo site_url('pages/news') ?>">News</a></li>
              <li><a href="<?php echo site_url('pages/how_to_play') ?>">An Introduction</a></li>
              <li><a href="<?php echo site_url('pages/getting_started') ?>">Getting Started</a></li>
              <li><a href="<?php echo site_url('pages/rules') ?>">Rules</a></li>
              <li><a href="<?php echo site_url('pages/contact') ?>">Feedback</a></li>
            </ul>

            <ul class="col-xs-12 col-sm-12 col-md-4 nav navbar-nav" id="user-nav">
              <?php if(!$this->tank_auth->is_admin()) { ?>
                  <li><a href="<?php echo site_url('games/picks') ?>">My Picks</a></li>
              <?php } ?>

              <li><a href="<?php echo site_url('games/standings') ?>">Leaderboards</a></li>
              <li><a href="<?php echo site_url('games/stats') ?>">User Stats</a></li>
              <?php if($this->tank_auth->is_admin()) { ?>
                <li class="dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin Menu <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('games/update_results') ?>">Update Results</a></li>
                    <li><a href="<?php echo site_url('games/create_games') ?>">Create Games</a></li>
                  </ul>
                </li>
              <?php } ?>  
            </ul> 

          </div> <!-- End Navbar Collapse -->

        </div> <!-- End Navbar -->

  </div>

</div> <!-- End Header -->
  
<div class="well">
	<p>
		At SevenCoals we care about your privacy and security. <b>We will not share your personal information.</b> We also promise to
		always ensure that this site is always safe for you to use, starting with the <b>SiteLock</b> seal of approval seen at the bottom of your screen.
		If you have any questions or concerns please feel free to contact us anytime.
	</p>
</div>


<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
?>
<?php echo form_open($this->uri->uri_string()); ?>
<table>
	<tr>
		<td><?php echo form_label('Password', $password['id']); ?></td>
		<td><?php echo form_password($password); ?></td>
		<td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
	</tr>
</table>
<?php echo form_submit('cancel', 'Delete account'); ?>
<?php echo form_close(); ?>
<?php $users = array(); ?>
<?php $points = array(); ?>
<?php $result = array(); ?>

<div class='panel panel-default col-md-12' id='games-list'>
		<h3 class='text-center'>Season Standings</h3>
		<table class='table table-striped'>

		<?php foreach($standings as $standings_info) {

			$username = $standings_info['username'];

			if(!in_array($username, $users))
			{
				array_push($users, $username);
			}
		} 

		foreach($users as $user) {

			$total_pts = 0;

			foreach($standings as $standings_info) {
				if($user === $standings_info['username'])
				{
					$total_pts = $total_pts + $standings_info['pts_earned'];
				}
			}

			array_push($points, $total_pts);
		}

		for($i=0;$i<count($users);$i++) {
			$result[$users[$i]] = $points[$i];
		}

		arsort($result);
		$position = 1;
		foreach($result as $user_key=>$points_value)
	    {
	    	if($user_key === $this->tank_auth->get_username()) {
		    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
			    		<td class='text-right'>".$position.".</td>
			    		<td>".$user_key."</td><td>".$points_value." points</td>
			    	</tr>";
			} else {
			    echo "<tr>
			    		<td class='text-right'>".$position.".</td>
			    		<td>".$user_key."</td><td>".$points_value." points</td>
			    	</tr>";
		    }
		    $position = $position + 1;
	    }
		?>

		</table>
</div>
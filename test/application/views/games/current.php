<?php $initialDate = FALSE; ?>
<?php $gameNumber = 0; ?>
<?php $numberOfGames = 0; ?>
<?php $no_games =TRUE; ?>

<div class='panel panel-default col-md-12' id='games-list'>
		<table class='table'>

			<?php foreach ($games as $game_info): ?>

				<?php 
				$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
				$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
				$currentDate = $game_info['date'];	
				$game_id = $game_info['id'];
				$gameNumber = $gameNumber + 1;
				$numberOfGames = $numberOfGames + 1;
				$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
				$awayPicked = FALSE;
				$homePicked = FALSE;
				$pickResult = 'empty';
				$teamPicked = 'team-picked';
				$pointsPossible = 0;
				$pointsEarned = 0;

				if ($game_info['status'] === 'closed') {

					$no_games = FALSE;

					foreach ($picks as $pick_info):

						if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') {
							$awayPicked = TRUE;
						} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') {
							$homePicked = TRUE;
						}

						if ($awayPicked === TRUE) {
							$pointsPossible = $game_info['homeVotes'];
						} else if ($homePicked === TRUE) {
							$pointsPossible = $game_info['awayVotes'];
						}

						if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === $game_info['winner']) {
							$pickResult = 'correct';
						} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] !== $game_info['winner'] && !empty($game_info['winner'])) {
							$pickResult = 'incorrect';
						}


					endforeach;

					if($initialDate === FALSE) {
						$previousDate = $currentDate; 
						echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
					} ?>

					<?php if($currentDate !== $previousDate) {
						echo "</table>";
						echo "</div>";
						echo "<div class='panel panel-default col-md-12' id='games-list'>";
						echo "<table class='table'>";
						echo "<div class='panel-heading text-center'>".date("l M. d, Y", strtotime($currentDate))."</div>";
					} ?>

					<tr>
						<td class="col-md-2"><?php echo date("g:iA", strtotime($game_info['time'])) ?></td>
						<td class='col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>"><br>
							</div>
							<?php echo $game_info['awayTeam'] ?>
						</td> 
						<td class='col-md-1 text-center'><br>@</td>
						<td class='col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
							<div id="logo-wrap">
								<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>"><br>
							</div>
							<?php echo $game_info['homeTeam'] ?>
						</td>
						<td class='col-md-3 text-center'
							 <?php if($pickResult === 'empty') { 
							 	echo "id='empty-pick'";
							 } else if ($pickResult === 'correct') {
							 	echo "id='correct-pick'";
							 } 
							 else if ($pickResult === 'incorrect') {
							 	echo "id='incorrect-pick'";
							 }?>>
							<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>
							<br>
							 	<?php echo "Playing for ".$pointsPossible." points."; 
							 ?>
						</td>
					</tr>

				<?php } ?>

				<?php $previousDate = $currentDate; ?>
				<?php $initialDate = TRUE; ?>

			<?php endforeach ?>

			<?php if($no_games === TRUE){echo "There aren't any current games to display.";} ?>

		</table>
</div>
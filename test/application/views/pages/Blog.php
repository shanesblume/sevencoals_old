       <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<?php if(!$this->tank_auth->is_logged_in()) { ?>
        		<a href="<?php echo base_url('auth/register') ?>" class="btn btn-primary btn-lg">Start Playing Today <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
       		<?php } else { ?>
        		<a href="<?php echo base_url('games/picks') ?>" class="btn btn-primary btn-lg">Go To My Picks <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        	<?php } ?>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container" id="content">

        <div class="row">

            <div class="col-md-8">
            	        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header" style="background:#8F4E0C;color:#FFFFFF;padding:.5em;">Game of The Week: Pittsburgh Penguins @ Chicago Blackhawks
                    <small>Feb 28 2014 by Nic Kanaar</small>
                </h1>
            </div>

        </div>
				<p>
				<div class="col-md-4 pull-right">
					<img class="img-responsive" src="http://upload.wikimedia.org/wikipedia/commons/a/ad/Sidney_Crosby_2010.jpg">
				</div>
				On March 1st two of NHL’s best teams go face-to-face for what should be an exciting game. The Chicago Blackhawks will host 
				the Pittsburgh Penguins outside at Soldier Field, and a clash of talent will be expected. With both teams sitting comfortably
				 at the top of their divisions, and with around 20 games to go in regular season, it’s safe to assume these two teams will make 
				 the playoffs. So, with nothing at stake, you’re probably wondering “who the hell cares?” Well, calm down you idiots, and let 
				 SevenCoals explain.
				</p>

				<p>
				Believe it or not, captain Sidney Crosby (Pittsburgh) and captain Jonathan Toews (Chicago) have not yet played against each other
				 since being drafted by their respected NHL teams. The last four times these two teams have met Crosby was injured. So this Saturday 
				 will be the first time these two elite players will go head-to-head. Captain vs. Captain, Titan vs. Titan, 
				 Letterman vs. Fallon, Sleep vs. House of Cards.
				</p>

				<p>
				The last time we saw the names Crosby and Toews in the same sentence was during the small controversy regarding who would wear the 
				covenanted “C” for team Canada during the Sochi Olympics. Crosby (who is leading the NHL in points) seemed like the obvious choice, but Toews
				 has the most Stanley Cup wins (2). Crosby got the spot in the end, but not until after the two verbally praised each other like a
				  pair of teenage girls trying on new clothes. This camaraderie between the two is even better for their matchup this Saturday.
				</p>

				<p>
				We here at SevenCoals get along with each other just fine, unless you bring out a ping-pong table. Put paddles in our hands and 
				it becomes a border battle between North and South Korea, with someone going home pissed and a little sweaty. We expect the same
				 results from Crosby and Toews this Saturday. The former Canadian teammates recognize each other’s talents, which is exactly why
				  they’ll want to outperform one another.
				</p>

				<p>
				As far as the actual game goes, you can’t decide what team to pick based solely on their team captains. Obviously there are other
				 names to consider, like: Kane, Sharp, Malkin, Kunitz, Crawford, and Fleury. Both teams are coming off losses from Thursday night
				  and will be thirsty for blood. Chicago has the better defense, but Pittsburgh has the better goalie. It will be up to Sidney Crosby
				   and Jonathan Toews to push their teammates to skate hard.
				</p>

				<p>
				A lot of people despise these two players for various reasons, but hate Captain Serious and Sid the Kid all you want, you can’t 
				deny they’re currently running this league. This will be an interesting pick for everyone playing our pick'em, and an even more 
				interesting game to watch.
				</p>

            </div>

            <div class="col-md-4">
                <h3 style="background:#8F1E0C;color:#FFFFFF;padding:.5em;">Update: Same Site, New Look<br><small>Mar 05 2014</small></h3>
					<p>
						Welcome to the new SevenCoals! We've spent weeks working this new design and hope everyone enjoys it! While it has delayed 
						some of our other projects (group creation, optional email alerts, etc) we made it a priority after reviewing how our users
						were visiting the site. This version should be much more mobile-friendly for those of you on the go and offer easier access 
						to the functions and information you use the most. There are still a few rough edges we will be cleaning up but couldn't wait
						 to show off the new design. If you have any questions or concerns be sure to give use the feedback form on the 
						home page email us at admin@sevencoals.com.
					</p>   

				<h3 style="background:#096A26;color:#FFFFFF;padding:.5em;">Prize Winner: djyang27<br><small>Mar 03 2014</small></h3>
					<p>
						Our first weekly prize winner is in the books, and congratulations to djyang27! You have a $10 Amazon.com gift card 
						on the way to your inbox. Thanks you everyone for playing last week and we wish you all the best of luck this week. 
						We are going to continue with the $10 gift card as a prize to this weeks champion. However, we are considering the 
						possibility of rewarding more positions as time goes on and as our numbers grow.
					</p>    

				<h3 style="background:#094759;color:#FFFFFF;padding:.5em;">In The Works: More Sports!<br><small>Mar 05 2014</small></h3>
					<p>
						It's hard to admit it but the NHL season is nearing an end. Don't worry! We will definitely be up and running through 
						playoffs, but we aren't stopping there. We are also hoping to continue with a World Cup pick'em in June and, fingers crossed,
						a possible March Madeness pick'em. Stay tuned for more updates!
					</p>           
            </div>

        </div>

	</div>	



 
 

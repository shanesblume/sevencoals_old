<?

class Pages_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_messages() 
	{
		date_default_timezone_set('America/New_York');

		$this->db->order_by('timestamp', 'desc');
		$query = $this->db->get('home_messages');

		return $query->result_array();
	}

	public function set_poll_result() 
	{
		date_default_timezone_set('America/New_York');

		$array = array(
			'username' => $this->input->post('username'),
			'poll_name' => $this->input->post('poll_name'),
			'result' => $this->input->post('result')
			);

		$this->db->insert('polls', $array);
	}

	

}

?>
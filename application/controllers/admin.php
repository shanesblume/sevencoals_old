<?php

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('games_model');
	}

	public function index()
	{
		if(!$this->tank_auth->is_logged_in() || !$this->tank_auth->is_admin()) {
			redirect('auth/login');
		}

		$data['title'] = 'SevenCoals Admin';

		$this->load->view('admin/header');
		$this->load->view('admin/footer');
	}

	public function create_games()
	{
		if(!$this->tank_auth->is_logged_in() || !$this->tank_auth->is_admin()) {
			redirect('auth/login');
		}

		$data['title'] = 'Create Games';

		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('time', 'Time', 'required');
		$this->form_validation->set_rules('awayTeam', 'Away Team', 'required');
		$this->form_validation->set_rules('homeTeam', 'Home Team', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('admin/header', $data);
			$this->load->view('admin/create_games');
			$this->load->view('admin/footer');
		}
		else
		{
			$this->games_model->set_games();
			redirect('admin/create_games');
		}
	}

}


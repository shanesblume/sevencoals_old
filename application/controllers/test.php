<?php

class Test extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
        $browser = new Buzz\Browser();
        $response = $browser->get('http://www.google.com');

        echo $browser->getLastRequest()."\n";
        echo $response;
    }
}

?>
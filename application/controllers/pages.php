<?php

class Pages extends CI_Controller {

	public function view($page = 'home') 
	{
		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			show_404();
		}

		$this->load->model('games_model');
		$this->load->model('pages_model');

		$data['title'] = ucfirst($page);
		$data['standings'] = $this->games_model->get_standings();
		$data['notables'] = $this->games_model->get_notable_picks();
		$data['games'] = $this->games_model->get_games();
		$data['messages'] = $this->pages_model->get_messages();
		$data['picks'] = $this->games_model->get_picks();
		$data['days'] = $this->games_model->get_week();

		$this->form_validation->set_rules('poll_name', 'Poll Name', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->pages_model->set_poll_result();
			redirect('pages/home');
		}

		
	}
}
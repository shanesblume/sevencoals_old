<?php

class Email extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->helper('url'); 
        $this->load->model('games_model');
        $data['leaderboard_title'] = 'Weekly';

    }

    public function index()
    {
        if(!$this->tank_auth->is_logged_in()) {
            redirect('auth/login');
        }

        $contact_message = $this->input->post('message');

        $config = Array(
                'protocol'  => 'smtp',
                'smtp_host' => 'gator4104.hostgator.com',
                'smtp_port' => '465',
                'smtp_user' => 'sblume@sevencoals.com',
                'smtp_pass' => 'Gizmo1345',
                'mailtype'  => 'html',
                'starttls'  => true,
                'newline'   => "\r\n"
            );

        $this->load->library('email', $config);
        $this->email->from($this->input->post('contact_type').'s@sevencoals.com', $this->input->post('username'));
        $this->email->to('admin@sevencoals.com');
        $this->email->subject($this->input->post('contact_type')." from user ".$this->input->post('username'));
        $this->email->message($this->input->post('message'));
        
        if(!empty($contact_message)) {
            $this->email->send();
            redirect('confirm_sent');
        } else {
            redirect('error_sent');
        }
        
        
    }

    public function confirm_sent()
    {
        if(!$this->tank_auth->is_logged_in()) {
            redirect('auth/login');
        }

        $data['games'] = $this->games_model->get_games();
        $data['picks'] = $this->games_model->get_picks();
        $data['standings'] = $this->games_model->get_standings();
        $data['results'] = $this->games_model->get_results();
        $data['notables'] = $this->games_model->get_notable_picks();
        $data['title'] = 'Results';

        $this->load->view('templates/header', $data);
        $this->load->view('pages/confirm_sent');
        $this->load->view('templates/footer');

    }

    public function error_sent()
    {
        if(!$this->tank_auth->is_logged_in()) {
            redirect('auth/login');
        }

        $data['games'] = $this->games_model->get_games();
        $data['picks'] = $this->games_model->get_picks();
        $data['standings'] = $this->games_model->get_standings();
        $data['results'] = $this->games_model->get_results();
        $data['notables'] = $this->games_model->get_notable_picks();
        $data['title'] = 'Results';

        $this->load->view('templates/header', $data);
        $this->load->view('pages/error_sent');
        $this->load->view('templates/footer');

    }


}
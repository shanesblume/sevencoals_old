              <li class="text-center"><h3>Notable Picks</h3></li>
              <table class='table table-striped'>

                <?php 

                $position = 1;
                $awayTeam = '';
                $homeTeam = '';

                foreach($notables as $notables_info) {
                  foreach($games as $game_info) {
                    if($game_info['id'] === $notables_info['game_id']) 
                    {
                      $notableAwayTeam = $game_info['awayTeam'];
                      $notableHomeTeam = $game_info['homeTeam'];
                      $notableGameDate = date('M. d', strtotime($game_info['date']));
                      $notableGameWinner = $game_info['winner'];

                      if($notableGameWinner === 'away') {
                        echo 
                        "<tr>
                          <td class='text-right'>".$position.". </td>
                          <td>".$notables_info['username']."<br>".$notables_info['pts_earned']." points</td>
                          <td>".$notableAwayTeam."* @ ".$notableHomeTeam."<br>".$notableGameDate."</td>
                        </tr>";
                      } else {
                        echo
                        "<tr>
                          <td class='text-right'>".$position.". </td>
                          <td>".$notables_info['username']."<br>".$notables_info['pts_earned']." points</td>
                          <td>".$notableAwayTeam." @ ".$notableHomeTeam."*<br>".$notableGameDate."</td>
                        </tr>";
                      }

                      $position = $position + 1;
                    }
                  }
                }

                 ?>;

              </table>

        </ul>

      </div>

</div>
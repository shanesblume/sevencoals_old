      <script type="text/javascript">
      ( function() {
          if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] } };

          var unit = {
              'publisher' : 'shanesblume',
              'width' : 300,
              'height' : 250,
              'sid' : 'Chitika First Placement'
          };

          var placement_id = window.CHITIKA.units.length;
          window.CHITIKA.units.push(unit);

          document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');

          var s = document.createElement('script');
          s.type = 'text/javascript';
          s.src = 'http://scripts.chitika.net/getads.js';

          try {
              document.getElementsByTagName('head')[0].appendChild(s);
          } catch(e) {
              document.write(s.outerHTML);
          }
      }());
  </script>
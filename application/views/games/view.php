<?php
$date_set = FALSE;
$this_users_pts = 0;
$position = 1;
$this_users_position = 1;

    	$parts=parse_url($_SERVER['REQUEST_URI']);
    	$path_parts=explode('/', $parts['path']);
        $target_date = $path_parts[count($path_parts)-1];
        $date_set = TRUE;

?>
    <div id="view-header" class="jumbotron">
      <div class="container">
        <h1>
          Daily Summary: <span style="color:#DC8124"><?php echo date("l M. d, Y", strtotime($target_date)) ?></span>
        </h1>

	    <row>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #8F1E0C">
	        		<span id="number">
	        			<?php $users = array(); ?>
						<?php $points = array(); ?>
						<?php $result = array(); ?>

					      <?php 
					      foreach($games as $game_info) 
					      {
					      	if(date('W',strtotime($game_info['date'])) === date('W',strtotime($target_date)))
					      	{
						        foreach($standings as $standings_info)
							    {
							          if($game_info['id'] === $standings_info['game_id'])
							          {
							            $username = $standings_info['username'];
							            if(!in_array($username, $users)) 
							            {
							               array_push($users, $username);
							            }
							           }
							         
							    }
							}
					      }

				          foreach($users as $user) {

				            $total_pts = 0;

				            foreach($games as $game_info) 
				            {
				            	if(date('W',strtotime($game_info['date'])) === date('W',strtotime($target_date))) 
				            	{
				              		foreach($standings as $standings_info)
				              		{
				                		if($user === $standings_info['username'] && $game_info['id'] === $standings_info['game_id'])
				                		{
				                			$total_pts = $total_pts + $standings_info['pts_earned'];
				                  		}
				                	}
				              	}
				            }
				            array_push($points, $total_pts);
				          }

						for($i=0;$i<count($users);$i++) {
							$result[$users[$i]] = $points[$i];
						}

						arsort($result);
						$position = 1;
						foreach($result as $user_key=>$points_value)
					    {
					    	if($user_key === $this->tank_auth->get_username()) {
					    		$this_users_pts = $points_value;
					    		$this_users_position = $position;
							}
						    $position = $position + 1;
					    }

					    echo $this_users_position;

						?>

	        		</span> 
	        	</div>
	        		Position
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #8F4E0C">
	        		<span id="number"><?php echo $this_users_pts ?></span>
	        	</div>	
	        		Points Earned
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #094759">
	        		<span id="number">
	        			<?php 
							$correct_picks = 0;
							$missed_picks = 0;

							foreach ($games as $game_info)
							{
								if(date('W',strtotime($game_info['date'])) === date('W'))
								{
									foreach($standings as $standings_info) 
									{
										if($standings_info['username'] === $this->tank_auth->get_username() 
											&& $game_info['id'] === $standings_info['game_id']
											&& $standings_info['result'] === '2')
										{
											$correct_picks = $correct_picks + 1;
										}

										if($standings_info['username'] === $this->tank_auth->get_username() 
											&& $game_info['id'] === $standings_info['game_id']
											&& $standings_info['result'] === '1')
										{
											$missed_picks = $missed_picks + 1;
										}
									}
								} 
							}

							$total_picks = $correct_picks + $missed_picks;
							if ($total_picks > 0) {
								$picks_percent = round(($correct_picks / $total_picks)*100);
							} else {
								$picks_percent = 0;
							}

							echo $picks_percent."%";

						?>
	        		</span>
	        	</div>
	        		Pick Accuracy
	        </div>

	        <div class="col-sm-3 col-xs-6" id="summary-wrap">
	        	<div id="summary-box" style="background: #096A26">
	        		<span id="number">
	        			<?php 
	        				$games_remaining = 0;
	        				foreach($games as $game_info)
	        				{
	        					if(date('W',strtotime($game_info['date'])) === date('W') && $game_info['status'] !== 'completed')
	        					{
	        						$games_remaining = $games_remaining + 1;
	        					}
	        				}
	        				echo $games_remaining;
	        			?>
	        		</span>
	        	</div>
	        		Games Remaining
	        </div>

        </row>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

	<?php echo form_open('games/view');

		$show_submit = TRUE;
		foreach($games as $game_info)
		{
			if($game_info['date'] < date('Y-m-d'))
			{
				$show_submit = FALSE;
			}
			elseif($game_info['date'] >= date('Y-m-d'))
			{
				$show_submit = TRUE;
			}
		}
		if($show_submit === TRUE)
		{ ?>
			
		<?php }
	?>

	<div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header" id="title-1">Day View
                    <small><?php echo date("l M. d, Y", strtotime($target_date)) ?></small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">

				<div id='picks-list'>
					<table class="col-xs-12 col-sm-12 col-md-12 table">
						<tr class="table-header">
							<td class="col-xs-2 col-sm-2 col-md-2 text-center">Time</td>
							<td class="col-xs-3 col-sm-3 col-md-3 text-center">Away</td>
							<td class="col-xs-1 col-sm-1 col-md-1 text-center">@</td>
							<td class="col-xs-3 col-sm-3 col-md-3 text-center">Home</td>
							<td class="col-xs-3 col-sm-3 col-md-3 text-center">Status</td>
						</tr>
					</table>

					<?php 
						foreach ($games as $key => $row) 
						{
							$date[$key]  = $row['date'];
							$time[$key] = $row['time'];
						} 
					?>

					<?php 

						$gameNumber = 0;
						$numberOfGames = 0;
						$no_games = TRUE;
						$date_list = array();
					?>

					<div id=''>

						<?php foreach ($games as $game_info) 
						{
							$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
							$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
							$currentDate = $game_info['date'];	
							$game_id = $game_info['id'];
							$gameNumber = $gameNumber + 1;
							$numberOfGames = $numberOfGames + 1;
							$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
							$awayPicked = FALSE;
							$homePicked = FALSE;
							$pickResult = 'empty';
							$teamPicked = 'team-picked';
							$pointsPossible = 0;

							if ($game_info['status'] === 'completed') 
							{
								foreach ($picks as $pick_info)
								{

									if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') 
									{
										$awayPicked = TRUE;
									} 

									else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') 
									{
										$homePicked = TRUE;
									}

									if ($awayPicked === TRUE) 
									{
										$pointsPossible = $game_info['homeVotes'];
									} 

									else if ($homePicked === TRUE) 
									{
										$pointsPossible = $game_info['awayVotes'];
									}

									if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === $game_info['winner']) 
									{
										$pickResult = 'correct';
									} 

									else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] !== $game_info['winner'] && !empty($game_info['winner'])) 
									{
										$pickResult = 'incorrect';
									}

								} ?>

								<table class='col-xs-12 col-sm-12 col-md-12 table'>

									<tr>

										<td class="col-xs-2 col-sm-2 col-md-2">
											<?php echo date("g:iA", strtotime($game_info['time'])) ?>
											<br>
											<span class='glyphicon glyphicon-check'></span>
											<p>Completed</p>
										</td>
										<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
											<div id="logo-wrap">
												<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>" class="img-responsive"><br>
											</div>
											<?php echo $game_info['awayTeam'] ?>
											<?php if($game_info['winner'] === 'away'){echo '*';} ?>
										</td> 
										<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>
										<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
											<div id="logo-wrap">
												<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>" class="img-responsive"><br>
											</div>
											<?php echo $game_info['homeTeam'] ?>
											<?php if($game_info['winner'] === 'home'){echo '*';} ?>
										</td>
										<td class='col-xs-3 col-sm-3 col-md-3 text-center'
											 <?php if($pickResult === 'empty') { 
											 	echo "id='empty-pick'";
											 } else if ($pickResult === 'correct') {
											 	echo "id='correct-pick'";
											 } 
											 else if ($pickResult === 'incorrect') {
											 	echo "id='incorrect-pick'";
											 }?>>
											 <?php if($pickResult === 'empty') { 
											 	echo "You didn't vote";
											 } else if ($pickResult === 'correct') {
											 	echo $pointsPossible."pts";
											 } 
											 else if ($pickResult === 'incorrect') {
											 	echo $pointsPossible."pts";
											 }?>
											 <br>
											<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>
										</td>

									</tr>

								</table>
						<?php }} ?>
					</div>


					<?php $gameNumber = 0; ?>
					<?php $numberOfGames = 0; ?>
					<?php $no_games =TRUE; ?>

					<div id=''>



						<?php 
							foreach ($games as $game_info)
							{
								$away_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png';
								$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
								$currentDate = $game_info['date'];	
								$game_id = $game_info['id'];
								$gameNumber = $gameNumber + 1;
								$numberOfGames = $numberOfGames + 1;
								$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
								$awayPicked = FALSE;
								$homePicked = FALSE;
								$pickResult = 'empty';
								$teamPicked = 'team-picked';
								$pointsPossible = 0;
								$pointsEarned = 0;

								if ($game_info['status'] === 'closed') {

									$no_games = FALSE;

									foreach ($picks as $pick_info)
									{
										if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') {
											$awayPicked = TRUE;
										} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') {
											$homePicked = TRUE;
										}

										if ($awayPicked === TRUE) {
											$pointsPossible = $game_info['homeVotes'];
										} else if ($homePicked === TRUE) {
											$pointsPossible = $game_info['awayVotes'];
										}

										if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === $game_info['winner']) {
											$pickResult = 'correct';
										} else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] !== $game_info['winner'] && !empty($game_info['winner'])) {
											$pickResult = 'incorrect';
										}
									} ?>

								<div id="picks-list">

									<table class='col-xs-12 col-sm-12 col-md-12 table'>

										<tr>

											<td class="col-xs-2 col-sm-2 col-md-2">
												<?php echo date("g:iA", strtotime($game_info['time'])) ?>
												<br>
												<span class='glyphicon glyphicon-play-circle'></span> 
												<p>In Progress</p>
											</td>

											<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
												<div id="logo-wrap">
													<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>" class="img-responsive"><br>
												</div>
												<?php echo $game_info['awayTeam'] ?>
											</td> 

											<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>

											<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
												<div id="logo-wrap">
													<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>" class="img-responsive"><br>
												</div>
												<?php echo $game_info['homeTeam'] ?>
											</td>

											<td class='col-xs-3 col-sm-3 col-md-3 text-center'
												 <?php if($pickResult === 'empty') { 
												 	echo "id='current-game'";
												 } else if ($pickResult === 'correct') {
												 	echo "id='current-game'";
												 } 
												 else if ($pickResult === 'incorrect') {
												 	echo "id='current-game'";
												 }?>>
												 <?php echo $pointsPossible."pts"; ?>
												 <br>
												<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>							 	
											</td>

										</tr>

									</table>

								</div>

						<?php }} ?>

					</div>


					<?php 

						$gameNumber = 0;
						$numberOfPicks = 0;
						$numberOfGames = 0;
						$todaysDate = date('Y-m-d');
						$todaysTime = date('H:i:s');

					?>

					<div id=''>


						<?php 

							foreach ($games as $game_info) 
							{

								$away_logo_url = base_url('/assets/img/team_logos').'/'.strtolower($game_info['awayTeam']).'_logo.png';
								$home_logo_url = base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png';
								$game_id = $game_info['id'];
								$gameNumber = $gameNumber + 1;
								$totalVotes = $game_info['awayVotes'] + $game_info['homeVotes'];
								$awayPicked = FALSE;
								$homePicked = FALSE;
								$teamPicked = 'team-picked';

								foreach ($picks as $pick_info) 
								{
									if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'away') 
									{
										$awayPicked = TRUE;
									} 
									else if ($game_id === $pick_info['game_id'] && $pick_info['pick'] === 'home') 
									{
										$homePicked = TRUE;
									}
								}

								if( $game_info['date'] >= $todaysDate && $game_info['status'] === 'open' ) 
								{
									$numberOfPicks = $numberOfPicks + 1;
									$numberOfGames = $numberOfGames + 1;
									echo form_hidden('username', $this->tank_auth->get_username());
									echo form_hidden('game_id_'.$gameNumber, $game_info['id']);		
									echo form_hidden('number_of_games', $numberOfGames);
									echo form_hidden('game_date_'.$gameNumber, date('Y-m-d', strtotime($game_info['date'])));
									echo form_hidden('game_time_'.$gameNumber, date('H:i:s', strtotime($game_info['time'])));

									?>

									<table class='col-xs-12 col-sm-12 col-md-12 table'>

										<tr>

											<td class="col-md-2">
												<?php echo date("g:iA", strtotime($game_info['time'])) ?>
												<br>
												<span class='glyphicon glyphicon-pencil'></span>
												<p>Still Open</p>
											</td>

											<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($awayPicked === TRUE) {echo $teamPicked;} ?>'>
												<label for="away_<?php echo $gameNumber ?>">
													<div id="logo-wrap">
														<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['awayTeam'].'_logo.png' ?>" class="img-responsive"><br>
													</div>
												</label><br>
												<input id="away_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="away" <?php if($awayPicked === TRUE) {echo "checked";} ?>>
												<?php echo $game_info['awayTeam'] ?>
											</td> 
											<td class='col-xs-1 col-sm-1 col-md-1 text-center'><br>@</td>
											<td class='col-xs-3 col-sm-3 col-md-3 text-center' id='<?php if($homePicked === TRUE) {echo $teamPicked;} ?>'>
												<label for="home_<?php echo $gameNumber ?>">
													<div id="logo-wrap">
														<img src="<?php echo base_url('/assets/img/team_logos').'/'.$game_info['homeTeam'].'_logo.png' ?>" class="img-responsive"><br>
													</div>
												</label><br>
												<input id="home_<?php echo $gameNumber ?>" type="radio" name="pick_<?php echo $gameNumber ?>" value="home" <?php if($homePicked === TRUE) {echo "checked";} ?>>
												<?php echo $game_info['homeTeam'] ?>
											</td>
											<td class='col-xs-3 col-sm-3 col-md-3 text-center' id="future-pick">
												<?php echo "Still open"; ?>
												<br>
												<?php echo $totalVotes ?> VOTE<?php if($totalVotes !== 1){echo "S";} ?>
											</td>

										</tr>

									</table>

							<?php }} ?>

					</div>

					<table class="table">
						<tr>
							<td class='col-xs-1 col-sm-1 col-md-4'></td>
							<td class='col-xs-10 col-sm-10 col-md-4 text-center'><?php if($numberOfPicks > 0 ) { echo '<button type="submit" class="btn btn-primary btn-lg" style="width:100%">Submit My Picks</button>' ;} ?></td>
							<td class='col-xs-1 col-sm-1 col-md-4'></td>
						</tr>
					</table>

				

			</div>

		</div>

		<div class="col-md-4">
			<div class="col-md-12 text-center">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- View - Right Sidebars -->
			<ins class="adsbygoogle"
			     style="display:inline-block;width:300px;height:250px"
			     data-ad-client="ca-pub-8036896959824733"
			     data-ad-slot="5575199204"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
			</div>

			<div>
				<h3 id="title-2">Key</h3>
	            <ul class="well">
	                <li style="background: #EE6D59">Red Status: Incorrect pick, no points earned.</li>
	                <li style="background: #AAE555">Green Status: Correct pick, points earned.</li>
	                <li style="background: #6DB1C4">Blue Status: Games in progress, voting closed.</li>
	                <li style="background: #FFFFFF">White Status: Game not started, voting open.</li>
	                <li style="background: #DBEFF5">Blue Behind Logo: Saved pick.</li>
	                <li style="">* By Team Name: Actual winner.</li>
	            </ul>
            </div>

            <div class="text-center"><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-lg" style="margin-bottom:1em;margin-top:1em;">View Daily Leaderboard</button></div>



            <h3 id="title-3">Note</h3>
            <p>
				Games may not update immediately after completion. We enter the game winners manually, which is typically done within 
				minutes of a game ending. Occassionally, some games (especialy the last games of the day) may not be updated 
				until the following morning. Thank you for your understanding.
			</p>

        </div>

        <?php echo form_close() ?>

	</div>

</div>






<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo"Point Totals for ".date("l M. d, Y", strtotime($currentDate)) ?></h4>
      </div>
      <div class="modal-body">
        
                <table class='table table-striped'>

                  <?php $users = array(); ?>
                  <?php $points = array(); ?>
                  <?php $result = array(); ?>

                  <?php foreach($standings as $standings_info) 
                  {
                    foreach($games as $game_info)
                    {
                      if($game_info['id'] === $standings_info['game_id'])
                      {
                        $username = $standings_info['username'];
                        if(!in_array($username, $users)) 
                        {
                           array_push($users, $username);
                        }
                      }
                     
                    }
                  }

                  foreach($users as $user) {

                    $total_pts = 0;

                    foreach($standings as $standings_info) 
                    {
                      foreach($games as $game_info)
                      {
                        if($game_info['id'] === $standings_info['game_id'])
                        {
                          if($user === $standings_info['username'])
                          {
                            $total_pts = $total_pts + $standings_info['pts_earned'];
                          }
                        }
                      }
                    }
                    array_push($points, $total_pts);
                  }

                  for($i=0;$i<count($users);$i++) {
                    $result[$users[$i]] = $points[$i];
                  }

				arsort($result);
				$position = 1;
				foreach($result as $user_key=>$points_value)
			    {
			    	if($user_key === $this->tank_auth->get_username()) {
				    	echo "<tr style='color:#D94D3F;font-weight:bold;'>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
					} else {
					    echo "<tr>
					    		<td class='text-right'>".$position.".</td>
					    		<td>".$user_key."</td><td>".$points_value." points</td>
					    	</tr>";
				    }
				    $position = $position + 1;
			    }
				?>

                </table>
                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





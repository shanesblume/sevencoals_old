<?php 
	if(!isset($week)) 
	{
		$week = date('W');
	}

	$week_range = date('F d',strtotime('2014W'.$week))." - ".date('F d',strtotime('+6 day', strtotime('2014W'.$week)));
?>

    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
        	Weekly Standings: <span style="color:#DC8124"><?php echo $week_range ?></span>
        </h1>

        <div class="col-md-12">

        	

			<?php echo form_open('games/standings');
					/* Dropdown Date Options */
					$week_options = array();
					foreach($games as $game_info) 
					{
						if(!in_array(date('W',strtotime($game_info['date'])), $week_options)) 
						{
                                                        $week_options[date('W',strtotime($game_info['date']))] = date('W',strtotime($game_info['date']));
						}
					}
				?>
				<h3 class="text-center">Select a different week: <?php echo form_dropdown('week_selected', $week_options, $week) ?></h3>
				<button type="submit" class="btn btn-primary" style="margin-top:20px;">Show Week</button>
				

			<?php echo form_close(); ?>

		</div>

      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->


    <div class="container" id="content">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Weekly Standings
                    <small><?php echo $week_range ?></small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
            	<?php $users = array(); ?>
				<?php $points = array(); ?>
				<?php $result = array(); ?>

				<?php 
				  foreach($games as $game_info) 
				  {
				  	if(date('W',strtotime($game_info['date'])) === date('W'))
				  	{
				        foreach($standings as $standings_info)
					    {
					          if($game_info['id'] === $standings_info['game_id'])
					          {
					            $username = $standings_info['username'];
					            if(!in_array($username, $users)) 
					            {
					               array_push($users, $username);
					            }
					           }
					         
					    }
					}
				  }
				  foreach($users as $user) {

				    $total_pts = 0;

				    foreach($games as $game_info) 
				    {
				    	if(date('W',strtotime($game_info['date'])) === date('W')) 
				    	{
				      		foreach($standings as $standings_info)
				      		{
				        		if($user === $standings_info['username'] && $game_info['id'] === $standings_info['game_id'])
				        		{
				        			$total_pts = $total_pts + $standings_info['pts_earned'];
				          		}
				        	}
				      	}
				    }
				    array_push($points, $total_pts);
				  }
				?>

				  <div class="mygrid-wrapper-div">
					  <table class="table table-striped table-responsive">

					  <?php 

						for($i=0;$i<count($users);$i++) {
							$result[$users[$i]] = $points[$i];
						}

						arsort($result);
						$position = 1;
						foreach($result as $user_key=>$points_value)
					    {
					    	if($user_key === $this->tank_auth->get_username()) {
						    	echo "<tr style='background:#6DB1C4;font-weight:bold;'>
							    		<td class='text-right'>".$position.".</td>
							    		<td>".$user_key."</td><td>".$points_value." points</td>
							    	</tr>";
							} else {
							    echo "<tr>
							    		<td class='text-right'>".$position.".</td>
							    		<td>".$user_key."</td><td>".$points_value." points</td>
							    	</tr>";
						    }
						    $position = $position + 1;
					    }
					  ?>

					  </table>
				  </div>
            </div>

            <div class="col-md-4">
                <h3>Tiebreaker Rules</h3>
				<ul>
					<li>Tiebreaker I: Most points for the week.</li>
					<li>Tiebreaker II: Most correct picks made that week.</li>
					<li>Tiebreaker III: Most points earned from a single pick that week.</li>
				</ul>               
            </div>

        </div>

	</div>	










<div id="rules">

<h4>Prizes:</h4>
<p>
Users must be 18 years or older to receive a weekly prize. Because the current prize is an electronic Amazon.com 
gift card, winning users will be sent a verification email to ensure that we have their correct contact information 
and that we aren't sending their prizes to a stranger. Prizes are subject to change but will be announced before the start 
of each week.
</p>

<h4>Dates and Deadlines:</h4>
<p>
Pick'em weeks start at midnight every Monday and end after the final game on Sunday. Games are counted for the day that 
they start on (using EST) not the day they end. Winners will be contacted within three days to verify their information. 
If you do not receive an email from us within three days and you believe you did win a prize, please contact us at sblume@sevencoals.com.
We are most likely having trouble getting in touch with you and have the wrong email address. If we are not able to confirm your email address
after 30 days you may no longer be eligible for that week's prize.
</p>

<h4>Playing the Game:</h4>
<p>
The goal of the pick'em is to end the week with as many points as possible. Points are earned by correctly picking the winning team of an NHL 
hockey game.
</p>
<p>
Incorrect picks will always earn zero points. If you pick a winner correctly you will receive 1 points for every user that picked the opposite
team to win that game. Ex: If 10 people pick the away team to win and 5 people pick the home team to win, a win by the home team would award the 
5 users who picked them 10pts each.  A win by the away team would award the 10 users who picked them 5pts each.
</p>
<p>
Picks can be made and changed from the time the game is posted (typically Sunday morningof the previous week) until the scheduled start time 
of the game. Whichever team you have selected to win at the scheduled start time will be locked in as your pick. If a game is postponed we will
typically erase that game and re-enter it on it's new date, meaning your pick for that game will also be erased. Just find the game on the new date 
and resubmit your pick.
</p>
<p>
Before a game starts you will only be able to see the total number of picks made for that game. You will not know how many points your are playing
for until after the game starts and all picks are locked in. This is done to reduce users from using calculated odds to make their picks. Our points 
structure is created to reward you for picking upsets (being one of the few people to pick an underdog will earn you more points than picking a team that 
everyone else also picked to win). We want to avoid Vegas-style picks (calculating a team's odds vs. the expected return if they win). Remember losses are 
worth 0 points every time. We've found that this A) Rewards hockey knowledge rather than gambling-like risk  and B) Makes for a more exciting experience when 
you check back during the games and find out exactly which games have the potential to make or break your day. With that all being said, feel free to pick underdogs 
for more points. There are many cases where the risk of picking an underdog is worth the reward if they win. We just want to avoid calculated risk.
</p>

<h4>Tiebreakers:</h4>
<p>
Winners are selected by the number of points they have at the end of the week. If users are tied at the end of the week a tiebreaker will be used to determine 
final positioning. 
</p>
<ul>
	<li>Tiebreaker I: Most points for the week.</li>
	<li>Tiebreaker II: Most correct picks made that week.</li>
	<li>Tiebreaker III: Most points earned from a single pick that week.</li>
</ul>
<p>
Final standings on the leaderboards may not reflect the tiebreaker process. It will only be used if needed to determine who receives a prize and is done 
manually. Leaderboards order users only by points (for now). If you find yourself in a prize position make sure you check to see if you are tied in points with anyone 
above or below you. If you are you will receive an email explaining the tiebreaker rules and results. If users are tied after the tiebeakers are applied 
we will typically go back and use previous weekly results (and the same tiebreakers) to determine the winner.
</p>

<h4>Errors:</h4>
<p>
If you notice an error in a game's information please notify us. If something is not correct we reserve the ability to make changes as necessary. If a game begins at 
8:00pm and we have it listed at 7:00pm, we will likely just change the time and keep all of the picks that were already made. If a game is listed with an incorrect team 
we would likely just delete it altogether. We will do our best to make sure it affects as few users as possible. We also reserve the right to disqualify or ban users that we 
believe are playing the game incorrectly. This may be due to having multiple accounts, inappropriate behavior, etc.
</p>

</div>






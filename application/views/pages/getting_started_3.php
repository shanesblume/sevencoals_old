
<div class="text-center" id="getting-started">

	<div class="row">
	<h4>Step 3: Day View</h4>
	<p>Here you'll see a list of all of the selected day's games. Click the logo of the team you think will win and choose 
	"Submit Picks" at the bottom of the page.</p>
	<img src="http://sevencoals.com/assets/img/gs_games.png" style="width:500px;">
	<p>When you submit a pick the team you selected to win will have a blue background behind it's logo. Notice that you can 
	also see game time and status on the left and the number of people who have voted on the game on the right.</p>
	<img src="http://sevencoals.com/assets/img/gs_games2.png" style="width:500px;">
	<p>You can change your pick for a game anytime before the game starts. White text in the "status" column means voting is
	 still open and your pick can be changed. Yellow text means the game has already started and voting is closed. Red or green text means 
	 the game is completed (red means you picked wrong, green means you picked correct).</p>
	 <p>Important: Until a game starts you can only see the number of votes placed so far. Once a game begins and voting is closed you will 
	 see the number of points you are playing for. Check back after a game starts to see how many points you are playing for. This is to prevent
	  users from purposely picking teams worth more or less points. (Read about how point values are calculated on the Introduction page).</p>
	<img src="http://sevencoals.com/assets/img/gs_games3.png" style="width:500px;">
	<br><br><a href="http://sevencoals.com/pages/getting_started_4">Go To Steps 4 and 5</a>
	</div>

</div>

<div class="text-center" id="getting-started">

	<div class="row">
	<h4>Step 2: Week View</h4>
	<p>Once your account is created it's time to make some picks! Go up to the navigation menu and look 
	for the "My Picks" link.</p>
	<img src="http://sevencoals.com/assets/img/gs_header2.png" style="width:500px;">
	<p>This will bring you to the a new page showing each day of the current week. On the right you will see some numbers. 
	The number to the right of the " / " is the total number of games being played for that day. The number before the " / "
	 is the number of games you've made a pick for. Green numbers mean you have made every pick for that day. Red numbers mean 
	 you have empty picks. Blue numbers mean that the day is closed and no more changes can be made. To view picks or results from 
	 one of the days click on the button in the far left column to go to the day view.</p>
	<img src="http://sevencoals.com/assets/img/gs_picks.png" style="width:500px;">
	<br><br><a href="http://sevencoals.com/pages/getting_started_3">Go To Step 3</a>
	</div>

</div>
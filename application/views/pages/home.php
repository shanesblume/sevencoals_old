    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<a href="<?php echo base_url('auth/register') ?>" class="btn btn-primary btn-lg">Start Playing Today <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container">
      <h3 id="features" class="subhead">Why Play SevenCoals Hockey Pick'em?</h3>
      <div class="row benefits">
        <div class="col-md-4 col-sm-6 benefit">
          <div class="benefit-ball">
            <span class="glyphicon glyphicon-usd"></span>
          </div>
          <h3>It's Free</h3>
          <p>Make your picks for every NHL game and compete for a weekly prize, all completely free. What's the catch? We can't get enough hockey. We love having a reason to be invested 
          in every single game and thought you should too.</p>
        </div> <!-- /.benefit -->

        <div class="col-md-4 col-sm-6 benefit">
          <div class="benefit-ball">
            <span class="glyphicon glyphicon-tower"></span>
          </div>
          <h3>It's Competitive</h3>
          <p>The games are weighted, but you don't know how much until they start. So how is it determined? A correct pick nets you a point for each person that picked the losing team to win.
          So the more people you outsmart, the more points you earn.</p>
        </div> <!-- /.benefit -->

        <div class="col-md-4 col-sm-6 benefit">
          <div class="benefit-ball">
            <span class="glyphicon glyphicon-gift"></span>
          </div>
          <h3>It's Rewarding</h3>
          <p>Let's ignore the fact that the Weekly Champion get's a $10 Amazon gift card, and focus on the feeling you get when you're in the 5% who picked an underdog to win.
           Okay fine, you can focus on the gift card.</p>
        </div> <!-- /.benefit -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->

    <div class="container-alternate">
      <div class="container">
        <h3 id="introduction" class="subhead">An Introduction</h3>
        <div class="row introduction">
          <div class="col-md-10 col-md-offset-1 text-center">
            <p>
              <strong>So What is SevenCoals?</strong> It's a free NHL hockey pick'em game with a unique scoring system 
              that rewards you for calling games that no one 
              else could. You pick the winners and we reward you with points and prizes. A new champion is crowned 
              every week so it's never too late to join. 
            </p>
			<p>
			  We've learned that there is more to hockey than watching your home team play on a channel that 
			  you can barely find. Or staring in awe as your fantasy hockey team get's destroyed by injuries and goalie changes.  
			  With SevenCoals you can have a vested interest in every single NHL game. Our complicated scoring algorithm* will let you 
			  know if you're a hockey predicting savant or just another Average Joe picking wins that everyone else saw coming. Get started by 
			  <a href="<?php echo site_url('auth/register') ?>">signing up</a> or <a href="<?php echo site_url('auth/login') ?>">signing in</a> and start making some picks! 
			</p>
			<p style="font-size:.9em">
				<em>*Complicated scoring algorithm is actually very simple, read more about it in the <a href="<?php echo site_url('pages/how_to_play') ?>">How to Play</a> section.</em> 
			</p>
          </div> <!-- /.col-md-10 -->
        </div> <!-- /.row -->
      </div> <!-- /.container -->
    </div> <!-- /.container-alternate -->

    <div class="container">
      <h3 id="faq" class="subhead">Frequently Asked Questions</h3>
      <div class="row faq">
        <p class="col-md-4 col-sm-6">
          <span id="head">What is a hockey pick'em?</span><br>
          In a hockey pick'em you try to predict which team is going to
			win an NHL hockey game based on research, hockey smarts, a gut feeling, or
			good ol' reliable luck. Your picks are matched 
			up against other players to see who does better.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">I've seen that before. Can you make it better?</span><br>
          We already did! With SevenCoals Hockey Pick'em every game is worth a variable 
				amount of points depending on how many people voted on a game and whether
				or not you are voting with the minority or the majority. If you correctly
				predict the winner of a game you get one point for each person that picked
				the losing team.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Sounds interesting, but I'm confused. Can you clarify that?</span><br>
          Absolutely. But try to keep up this time, or read the detailed list of rules further down. 
          Let's say the Buffalo Sabres are playing the Pittsburgh Penguins. 
          		In total 100 people made picks for that game. And out of those 100 picks submitted, 80 users picked 
				Pittsburgh as the winner while the other 20 people picked Buffalo.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Wait... stop right there. Do the people who picked Buffalo even watch hockey?</span><br>
          It's just an example. If Pittsburgh wins, anyone who picked 
				them would get 20pts (because 20 people picked Buffalo) and everyone who picked Buffalo 
				would get 0pts (because they were wrong). But if Buffalo wins the 20 people that picked them
				would get 80pts each (beacuse 80 people picked Pittsburgh) and the 80 people that 
				picked Pittsburgh would get 0pts each (beacuse they were wrong).
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Okay, so I should always vote for the underdog?</span><br>
          Not at all. Even though you don't lose any points for an incorrect pick, you don't
				gain any either. You'll have to decide if the reward of more points is worth the 
				risk of getting none at all. Oh, and one more thing. You cannot see the spread of the votes until
				the game starts. So if everyone is trying to pick the underdog they could actually
				end up being worth less! Be sure to check back during games (once picks are locked) to see how many points your 
				games are worth.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Okay, I think I'm getting it. But why do it at all?</span><br>
          A few reasons. First it gives you a vested interest in NHL games that you typically wouldn't
				be as interested in. Also, there's the fame and fortune. We declare a champion each
				week based on a running point total for that week (ending after Sunday's games). This
				will ensure that tens of people are aware of your brilliant hockey brain. We are also currently offering 
				a weekly prize to the best of the best.
        </p>
      </div> <!-- /.faqs -->
    </div> <!-- /.container -->

    <div class="container-alternate" id="rules-container">
      <div class="container">
        <h3 id="rules" class="subhead">Detailed Rules</h3>
        <div class="row rules">
          <div class="col-md-10 col-md-offset-1 text-left">
            <h4>Prizes:</h4>
				<p>
				Users must be 18 years or older to receive a weekly prize. Because the current prize is an electronic Amazon.com 
				gift card, winning users will be sent a verification email to ensure that we have their correct contact information 
				and that we aren't sending their prizes to the wrong person. Prizes are subject to change but will be announced before the start 
				of each week.
				</p>

				<h4>Dates and Deadlines:</h4>
				<p>
				Pick'em weeks start at midnight every Monday and end after the final game on Sunday. Games are counted for the day that 
				they start on (using EST) not the day they end. Winners will be contacted within three days to verify their information. 
				If you do not receive an email from us within three days and you believe you did win a prize, please contact us at sblume@sevencoals.com.
				We are most likely having trouble getting in touch with you and have the wrong email address. If we are not able to confirm your email address
				after 30 days you may no longer be eligible for that week's prize.
				</p>

				<h4>Playing the Game:</h4>
				<p>
				The goal of the pick'em is to end the week with as many points as possible. Points are earned by correctly picking the winning team of an NHL 
				hockey game.
				</p>
				<p>
				Incorrect picks will always earn zero points. If you pick a winner correctly you will receive 1 points for every user that picked the opposite
				team to win that game. Ex: If 10 people pick the away team to win and 5 people pick the home team to win, a win by the home team would award the 
				5 users who picked them 10pts each.  A win by the away team would award the 10 users who picked them 5pts each.
				</p>
				<p>
				Picks can be made and changed from the time the game is posted (typically Sunday morningof the previous week) until the scheduled start time 
				of the game. Whichever team you have selected to win at the scheduled start time will be locked in as your pick. If a game is postponed we will
				typically erase that game and re-enter it on it's new date, meaning your pick for that game will also be erased. Just find the game on the new date 
				and resubmit your pick.
				</p>
				<p>
				Before a game starts you will only be able to see the total number of picks made for that game. You will not know how many points your are playing
				for until after the game starts and all picks are locked in. This is done to reduce users from using calculated odds to make their picks. Our points 
				structure is created to reward you for picking upsets (being one of the few people to pick an underdog will earn you more points than picking a team that 
				everyone else also picked to win). We want to avoid Vegas-style picks (calculating a team's odds vs. the expected return if they win). Remember losses are 
				worth 0 points every time. We've found that this A) Rewards hockey knowledge rather than gambling-like risk  and B) Makes for a more exciting experience when 
				you check back during the games and find out exactly which games have the potential to make or break your day. With that all being said, feel free to pick underdogs 
				for more points. There are many cases where the risk of picking an underdog is worth the reward if they win. We just want to avoid calculated risk.
				</p>

				<h4>Tiebreakers:</h4>
				<p>
				Winners are selected by the number of points they have at the end of the week. If users are tied at the end of the week a tiebreaker will be used to determine 
				final positioning. 
				</p>
				<ul>
					<li>Tiebreaker I: Most points for the week.</li>
					<li>Tiebreaker II: Most correct picks made that week.</li>
					<li>Tiebreaker III: Most points earned from a single pick that week.</li>
				</ul>
				<p>
				Final standings on the leaderboards may not reflect the tiebreaker process. It will only be used if needed to determine who receives a prize and is done 
				manually. Leaderboards order users only by points (for now). If you find yourself in a prize position make sure you check to see if you are tied in points with anyone 
				above or below you. If you are you will receive an email explaining the tiebreaker rules and results. If users are tied after the tiebeakers are applied 
				we will typically go back and use previous weekly results (and the same tiebreakers) to determine the winner.
				</p>

				<h4>Errors:</h4>
				<p>
				If you notice an error in a game's information please notify us. If something is not correct we reserve the ability to make changes as necessary. If a game begins at 
				8:00pm and we have it listed at 7:00pm, we will likely just change the time and keep all of the picks that were already made. If a game is listed with an incorrect team 
				we would likely just delete it altogether. We will do our best to make sure it affects as few users as possible. We also reserve the right to disqualify or ban users that we 
				believe are playing the game incorrectly. This may be due to having multiple accounts, inappropriate behavior, etc.
				</p>

				</div>
          </div> <!-- /.col-md-10 -->
        </div> <!-- /.row -->
      </div> <!-- /.container -->
    </div> <!-- /.container-alternate -->

    <div class="container">
      <h3 id="getting-started" class="subhead">How to Get Started</h3>
      <div class="row getting-started">
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 1: Register</span><br>
          When you're ready to get started click on "Register" 
			in the menu at the top of the page. If you've already created a profile, click "Login".
			To create an account enter your desired username and an email address. We do not share
			 your email or other information with anyone. Be sure to check your inbox for an email 
			 from us after your account is completed. You do not have to verify your email address but
			 if any prizes are won this is how we will contact you and confirm delivery information.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 2: Week View</span><br>
          Once your account is created it's time to make some picks! Go up to the navigation menu and look 
			for the "My Picks" link.
			This will bring you to a new page showing each day of the current week. In the last column, green numbers mean you have made every pick for that day. Red numbers mean 
			 you have empty picks (and that's a bad thing). To make picks or view results for 
			 one of the days click on the button in the far left column to go to the day view.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 3: Day View</span><br>
          Here you'll see a list of all of the selected day's games. Click the logo of the team you think will win and choose 
			"Submit Picks" at the bottom of the page.
			When you submit a pick the team you selected will have a blue background behind it's logo. Notice that you can 
			also see game time and status on the left and the number of people who have voted on the game on the right.
			You can change your pick for a game anytime before the game starts. White text in the "status" column means voting is
			 still open and your pick can be changed. 
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 4: Research and Adjust (optional)</span><br>
          If you are struggling with a game you have two options: go with your gut or hit the books. To help you out a little we also offer 
          occasional tips and game previews. While we try our best to never recommend one team or another, we do like to give you some inside 
          information that could help you make smarter picks. Even better is that these articles are often written by our friend Nic Kanaar who 
          has a brilliant gift for witty, clever, and humorous writing. Whether you're undecided on a game, want to know what's what in the hockey world, 
          or are just looking for a laugh, be sure to check out his articles often on our Blog page.
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 5: Check Back After the Puck Drops</span><br>
          Back in the game View, yellow text in the "Game Status" means the game has already started and voting is closed. This also means 
          that the pick has been locked. This is our favorite part of the night because you can see how many points your picks
           are worth. Everyone picked BUF over PIT looking for an upset? <em>Sorry hun, no dinner with your parents tonight. I've 
           got a game to watch!</em> Once the game is completed the numbers will turn green (if you guessed correct) or red (if you were wrong).
        </p>
        <p class="col-md-4 col-sm-6">
          <span id="head">Step 6: Size up the Competition</span><br>
          You can use the Standings page to track how well you are doing. The initial screen will show you the running totals
           for the current week. There will also be a tab to pull up last weeks results. And while SevenCoals is set up to be played week-by-week, 
          you can also view your season totals here. And dont forget the User Stats page. Here you can see a lot of interesting stats about 
          your picks (and other user's picks also). You'll be able to track how often you are correct, whether you tend to take risks or 
          pick safe, and what kind of teams you gravitate to. If things just aren't working out this is a good place to see what you can 
          change (or see what the winners are doing differently). 
        </p>
      </div> <!-- /.faqs -->
    </div> <!-- /.container -->

    <div class="container-alternate" id="feedback-container">
      <div class="container">
        <h3 id="feedback" class="subhead">Give Us Your FeedBack!</h3>
        <div class="row feedback">
          <div class="col-md-10 col-md-offset-1 text-center">
            	<?php echo form_open('email'); ?>

					<input type="hidden" name="username" value="<?php echo $this->tank_auth->get_username() ?>">

					<div>

						<div class="row">
							<span class='text-center'><h4>Have a comment, question, or suggestion for SevenCoals? We want to hear it!</h4></span>
							
							<?php 
							$contact_type = array(
			                  'comment'    => "I'd like to make a comment: ",
			                  'suggestion'   => "I have a suggestion for the site: ",
			                  'question'  => "I have a question I need answered: ",
			                  'report'   => "Something's broken, fix it: ",
			                );


			       			echo form_dropdown('contact_type', $contact_type);
							

							echo "<br><br><br>";

							?>

							<textarea class="col-xs-12 col-sm-12 col-md-12" rows="7" name="message"></textarea>
							<span class="pull-right">Be sure to include your email if you'd like a reply! <button type="submit" class="btn btn-picks">Send</button></span>

						</div>

						<hr>

					</div>

	<?php echo form_close() ?>
          </div> <!-- /.col-md-10 -->
        </div> <!-- /.row -->
      </div> <!-- /.container -->
    </div> <!-- /.container-alternate -->



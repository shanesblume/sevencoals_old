    <div id="top" class="jumbotron">
      <div class="container">
        <h1>
          SevenCoals Hockey Pick'em: <span style="color:#DC8124">It's the way a pick'em <em>should</em> be played.</span>
        </h1>
        <h2>And let's admit it, your fantasy team is a mess...</h2>
        <p>
        	<?php if(!$this->tank_auth->is_logged_in()) { ?>
        		<a href="<?php echo base_url('auth/register') ?>" class="btn btn-primary btn-lg">Start Playing Today <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
       		<?php } else { ?>
        		<a href="<?php echo base_url('games/picks') ?>" class="btn btn-primary btn-lg">Go To My Picks <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        	<?php } ?>
        </p>
      </div> <!-- /.container -->
    </div> <!-- /.jumbotron -->

    <div class="container" id="content">

        <div class="row" id="blog-content">

            <div class="col-md-8">

    	        <div class="row">

                    <div class="col-lg-12">
                        <h1 class="page-header" id="title-1">Sunday Headlines
                            <small>March 15 2014 by Nic Kanaar</small>
                        </h1>
                    </div>

                </div>

				<div class="col-md-4 pull-right"><figure><img class="img-responsive" src="http://upload.wikimedia.org/wikipedia/commons/4/46/Phil_Kessel_smiles_for_the_crowd_Toronto_Ontario_2010.jpg">
<figcaption>Toronto's Phil Kessel loves a good Kuznetsov joke. But then again, who doesn't?</figcaption></figure></div>

                <p>
This Sunday, March 16th, is jam-packed with more games than usual. These nine games serve as an opportunity for many of the SevenCoals Pick’em players to either catchup or enhance their leads. These kinds of days can shake up the leaderboard allowing players to swing up to the top or plummet to an embarrassing finish. So if you’re one of the idiots who keeps forgetting to make their picks, or someone who has a knack for picking the losing team, this Sunday is for you. To help aid, but not persuade, you with your picks, SevenCoals has made up some clever headlines so you know what to consider when deciding on a winner.
                </p>
                 
<h3><small>Maple Leafs vs. Capitals</small><br> <cite>Can Kessel Keep Kuznetsov Calm on Ksunday?</cite></h3>
<p>
Toronto’s looking good as we near the regular season finish line, but Washington’s right outside the playoff bubble and newcomer Evgeny Kuznetsov is starting to find his legs with his new team.
                </p>

<h3><small>Flyers vs. Penguins</small><br> <cite>Flyers and Penguins Race to Perfect 10 (expected goals) in Diving Competition.</cite></h3>
<p> 
No explanation needed here. These two teams are good division rivals with Philadelphia scratching to stay in playoff contention. Drink every time Crosby or Hartnell hit the ground?
                </p>

<h3><small>Oilers vs. Hurricanes</small><br> <cite>Thousands to Attend Military Appreciation Event in Carolina. Hockey Game May Also Entertain.</cite></h3>
<p>
With Edmonton at the bottom of their division, and Carolina just hovering in second to last in theirs, this pick will be all about who’s the better of the worst at the Hurricane's Military Appreciation Night.
                </p>

<h3><small>Canucks vs Panthers</small><br> <cite>Will Vancouver <em>Lack</em> Against Luongo?</cite></h3> 
<p>
Luongo will be facing his former Vancouver team, and the Canucks are a couple games back from making the playoffs. This combination should make for an awesome game as both teams have something on the line.
                </p>

<h3><small>Stars vs. Jets</small><br> <cite>Who Has the Biggest Heart, the Jets or <strike>Peverly</strike> the Stars?</cite></h3>
<p>
With Rich Peverly out for the season after nearly dying of cardiac arrest, this Dallas team is left without one of their strongest centers. But against a struggling Winnipeg team this should make for an interesting game. 
                </p>

                <div class="text-center pull-left" style="padding:2em;">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blog - Article -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:180px;height:150px"
                             data-ad-client="ca-pub-8036896959824733"
                             data-ad-slot="9785072800"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>

<h3><small>Red Wings vs Hawks</small><br> <cite>Former Calder Trophy Winner Patrick Kane Takes on the Calder Cup Champs.</cite></h3>
<p>
When these two teams clash it’s always a fun and bone-crushing time. Chicago’s been a better team, but Detroit (starting numerous former AHL champions due to injuries) is desperate for some wins. Expect fireworks when the puck drops.
                </p>

<h3><small>Sharks vs Rangers</small><br> <cite>Bracken Kearns May or May Not, or May, Be Asked to Help Sharks Take on Rookie Haggerty and the Rangers.</cite></h3>
<p>
It’s been comical to watch San Jose reassigning Kearns left and right, and to hear the Rangers praise their new messiah Ryan Haggerty. But no matter the distraction these two teams are almost evenly matched. 
                </p>

<h3><small>Avalanche vs. Senators</small><br> <cite>Senators Willing to Fist Fight Avalanche Head Coach Patrick Roy in Attempt to Lower Team Morale.</cite></h3>
<p>
Colorado is on fire, but Ottawa needs some wins to move into a Wild Card spot in the standings. And they've proven that they'll 
scrap to get them.
                </p>

<h3><small>Canadiens vs. Sabres</small><br> <cite>Sabres Report That They’ll Try to Pencil in a Game Against the Canadiens this Sunday.</cite></h3> 
<p>
Montreal needs a win to compete with Division rivals Tampa Bay, and Buffalo needs a win to, well, just get more than 19 wins on the season.
                </p>



                <hr>

            </div>

            <div class="col-md-4">   

                <div class="col-md-12 text-center">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blog - Right Sidebar -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:300px;height:250px"
                             data-ad-client="ca-pub-8036896959824733"
                             data-ad-slot="5494474003"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div> 

                <?php if( date('l') === 'Sunday') { ?>

                    <h3>Next Week's Picks are Open!</h3>
                        <p>Whether you're a weekend warrior or just trying to squeeze as much hockey as possible into your 
                        weekend, we know you may want to make next week's pick now. Well it's Sunday, you have free time, and 
                        you may not before games start tomorrow. So we are giving you exclusive early access (along with everyone else)
                        to next week's picks.
                        </p>

                        <div class="text-center"><a href="<?php echo base_url('games/next_weeks_picks') ?>"><button type="submit" class="btn btn-primary btn-lg">Make Next Week's Picks</button></a></div>

                <?php } ?>

                <h3 id="title-2">Prize Winner: lblume<br><small>Mar 17 2014</small></h3>
                <p>
                    Congratulations to our newest champion, lblume! Keep a lookout for a $10 amazon gift card which is on the way to your inbox. Thank you everyone for playing 
                    last week and we wish you all the best of luck this week. We are going to continue with the $10 gift card as 
                    a prize to this weeks champion. However, we are considering the possibility of rewarding more positions as 
                    time goes on and as our numbers grow.
                </p>  

                <h3 id="title-3">Update: Same Site, New Look<br><small>Mar 05 2014</small></h3>
                <p>
                    Welcome to the new SevenCoals! We've spent weeks working this new design and hope everyone enjoys it! While it has delayed 
                    some of our other projects (group creation, optional email alerts, etc) we made it a priority after reviewing how our users
                    were visiting the site. This version should be much more mobile-friendly for those of you on the go and offer easier access 
                    to the functions and information you use the most. There are still a few rough edges we will be cleaning up but we couldn't wait
                     to show off the new design. If you have any questions or concerns be sure to use the feedback form on the 
                    home page email or us at admin@sevencoals.com.
                </p> 
           
            </div>

        </div>

	</div>	



 
 
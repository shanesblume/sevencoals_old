<?php

class Games_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_games($date_slug = FALSE)
	{
		if($date_slug === FALSE)
		{
			$this->db->order_by("date", "asc");
			$this->db->order_by("time", "asc");
			$query = $this->db->get('games');
			return $query->result_array();
		}

		$query = $this->db->get_where('games', array('date' => $date_slug));
		return $query->result_array();
	}

	public function set_games()
	{
		$id = url_title($this->input->post('id'), 'dash', TRUE);

		$data = array(
			'date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			'awayTeam' => $this->input->post('awayTeam'),
			'homeTeam' => $this->input->post('homeTeam'),
			'status' => $this->input->post('status'),
			'id' => $id
		);

		return $this->db->insert('games', $data);
	}


	public function get_picks()
	{
		$array = array (
				'username' => $this->tank_auth->get_username(),
			);

		$query = $this->db->get_where('picks', $array);

		return $query->result_array();
	}

	public function get_user_picks($user_slug)
	{
		$query = $this->db->get_where('picks', array('username' => $user_slug));
		return $query->result_array();
	}


	public function get_users()
	{
		$array = array (
				'group_id' => '300',
			);

		$this->db->order_by("UPPER(username)", "asc"); 
		$query = $this->db->get_where('users', $array);

		return $query->result_array();
	}

	public function get_standings()
	{
		$query = $this->db->get('picks');

		return $query->result_array();
	}

	public function set_picks()
	{
			
			date_default_timezone_set('America/New_York');

			$number_of_games = $this->input->post('number_of_games');

			for($i=1; $i<=($number_of_games+20); $i++) {

				$pick = $this->input->post('pick_'.$i);

				if( !empty($pick) && 
					( ( $this->input->post('game_date_'.$i) > date('Y-m-d') ) || 
					( $this->input->post('game_date_'.$i) === date('Y-m-d') && $this->input->post('game_time_'.$i) > date('H:i:s') ) )
				  ) {

					$data[$i] = array (
						'username' => $this->input->post('username'),
						'game_id' => $this->input->post('game_id_'.$i),
						'pick' => $this->input->post('pick_'.$i)
						);
					$array[$i] = array(
						'username' => $this->input->post('username'),
						'game_id' => $this->input->post('game_id_'.$i),
						);

					$query = $this->db->get_where('picks', $array[$i]);

					if ($query->num_rows() > 0) {

						$this->db->where('game_id', $this->input->post('game_id_'.$i));
						$this->db->where('username', $this->input->post('username'));
						$this->db->update('picks', $data[$i]);

					} else {

						$this->db->insert('picks', $data[$i]);

					}

					$this->db->where('game_id', $this->input->post('game_id_'.$i));
					$this->db->where('pick', 'home');
					$this->db->from('picks');
					$homeVotes = $this->db->count_all_results();
					$updateHomeVotes = array ('homeVotes' => $homeVotes);
					$this->db->where('id', $this->input->post('game_id_'.$i));
					$this->db->update('games', $updateHomeVotes);

					$this->db->where('game_id', $this->input->post('game_id_'.$i));
					$this->db->where('pick', 'away');
					$this->db->from('picks');
					$awayVotes = $this->db->count_all_results();
					$updateAwayVotes = array ('awayVotes' => $awayVotes);
					$this->db->where('id', $this->input->post('game_id_'.$i));
					$this->db->update('games', $updateAwayVotes);

				}
			}
	}

	public function get_results() 
	{

		$this->db->order_by("date", "asc"); 
		$query = $this->db->get('games');
		return $query->result_array();
		
	}

	public function update_results()
	{
			$number_of_games = $this->input->post('number_of_games');

			for($i=1; $i<=$number_of_games; $i++) {

				$result = $this->input->post('result_'.$i);

				if(!empty($result)) {

					$data[$i] = array (
						'winner' => $this->input->post('result_'.$i),
						'status' => 'completed',
						);

					$this->db->where('id', $this->input->post('game_id_'.$i));
					$this->db->update('games', $data[$i]);

					$array1[$i] = array (
						'result' => 1
						);

					$this->db->where('game_id', $this->input->post('game_id_'.$i));
					$this->db->update('picks', $array1[$i]);

					$array2[$i] = array (
						'result' => 2
						);

					$this->db->where('game_id', $this->input->post('game_id_'.$i));
					$this->db->where('pick', $this->input->post('result_'.$i));
					$this->db->update('picks', $array2[$i]);

					$query = $this->db->get_where('games', array('id' => $this->input->post('game_id_'.$i)));
					$row = $query->row_array();
					if($this->input->post('result_'.$i) === 'away') {
						$pts_earned = $row['homeVotes'];
					} elseif($this->input->post('result_'.$i) === 'home'){
						$pts_earned = $row['awayVotes'];
					}

					$array3[$i] = array (
						'pts_earned' => $pts_earned
						);

					$this->db->where('game_id', $this->input->post('game_id_'.$i));
					$this->db->where('pick', $this->input->post('result_'.$i));
					$this->db->update('picks', $array3[$i]);

				}
			}
	}

	public function update_game_status() 
	{
		date_default_timezone_set('America/New_York');

		$date= date('Y-m-d');
		$time= date('H:i:s');

		$data = array (
			'status' => 'open'
			);

		$this->db->where('status', 'unavailable');
		$this->db->where('date', $date);
		$this->db->or_where('date', date("Y-m-d", strtotime("$date +1 day")));
		$this->db->or_where('date', date("Y-m-d", strtotime("$date +2 day")));
	 	$this->db->update('games', $data);

	 	$data = array (
	 		'status' => 'closed'
	 		);

	 	$this->db->where('status', 'open');
		$this->db->where('date <=', $date);
		$this->db->where('time <=', $time);
		$this->db->update('games', $data);


	}

	public function get_notable_picks() 
	{
		$this->db->order_by('pts_earned', 'desc');
		$this->db->limit(3);
		$query = $this->db->get('picks');

		return $query->result_array();
	}

	public function get_week() 
	{
		date_default_timezone_set('America/New_York');
		
		$this->db->order_by('date', 'asc');
		$this->db->order_by('time', 'asc');
		$query = $this->db->get('games');

		return $query->result_array();
	}

}

